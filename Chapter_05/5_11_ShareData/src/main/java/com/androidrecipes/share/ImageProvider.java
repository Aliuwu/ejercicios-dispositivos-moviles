package com.androidrecipes.share;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;

import java.io.FileNotFoundException;
import java.io.IOException;

public class ImageProvider extends ContentProvider {

    public static final Uri CONTENT_URI = Uri.parse("content://com.examples.share.imageprovider");

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "nameString";
    public static final String COLUMN_IMAGE = "imageUri";
    private static final String[] DEFAULT_PROJECTION = {
            COLUMN_ID, COLUMN_NAME, COLUMN_IMAGE
    };

    private String[] mNames, mFilenames;

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("This ContentProvider is read-only");
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        throw new UnsupportedOperationException("This ContentProvider is read-only");
    }

    @Override
    public boolean onCreate() {
        mNames = new String[]{"John Doe", "Jane Doe", "Jill Doe"};
        mFilenames = new String[]{"logo1.png", "logo2.png", "logo3.png"};
        return true;
    }

    /**
     * Devuelve todas las columnas si no se ha dado una proyección
     * @param uri
     * @param projection
     * @param selection
     * @param selectionArgs
     * @param sortOrder
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (projection == null) {
            projection = DEFAULT_PROJECTION;
        }
        MatrixCursor cursor = new MatrixCursor(projection);

        for (int i = 0; i < mNames.length; i++) {
            //Añade las columnas solicitadas
            MatrixCursor.RowBuilder builder = cursor.newRow();
            for (String column : projection) {
                if (COLUMN_ID.equals(column)) {
                    //Usa el índice del arreglo como identificador
                    builder.add(i);
                }
                if (COLUMN_NAME.equals(column)) {
                    builder.add(mNames[i]);
                }
                if (COLUMN_IMAGE.equals(column)) {
                    builder.add(Uri.withAppendedPath(CONTENT_URI, String.valueOf(i)));
                }
            }
        }
        return cursor;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("This ContentProvider is read-only");
    }

    /**
     * Devuelve el asset apropiado para el elemento solicitado
     * @param uri
     * @param mode
     * @throws FileNotFoundException
     */
    @Override
    public AssetFileDescriptor openAssetFile(Uri uri, String mode) throws FileNotFoundException {
        int requested = Integer.parseInt(uri.getLastPathSegment());
        AssetFileDescriptor afd;
        AssetManager manager = getContext().getAssets();
        try {
            switch (requested) {
                case 0:
                case 1:
                case 2:
                    afd = manager.openFd(mFilenames[requested]);
                    break;
                default:
                    afd = manager.openFd(mFilenames[0]);
                    break;
            }
            return afd;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
