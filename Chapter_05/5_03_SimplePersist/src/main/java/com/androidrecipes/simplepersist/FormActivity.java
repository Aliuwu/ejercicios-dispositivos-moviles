package com.androidrecipes.simplepersist;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

/**
 * Problema: La aplicación necesita un método simple para almacenar datos básicos como números y Strings
 * Solcuión: Usaremos un objeto de SharedPreferences para poder crear espacios en los que los datos puedan ser almacenados
 */
public class FormActivity extends Activity implements View.OnClickListener {

    EditText email, message;
    CheckBox age;
    Button submit;

    SharedPreferences formStore;

    boolean submitSuccess = false;

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form);

        email = (EditText) findViewById(R.id.email);
        message = (EditText) findViewById(R.id.message);
        age = (CheckBox) findViewById(R.id.age);

        submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(this);

        //Crea el objeto de preferences
        formStore = getPreferences(Activity.MODE_PRIVATE);
    }

    /**
     * Restaura los datos
     */
    @Override
    public void onResume() {
        super.onResume();
        email.setText(formStore.getString("email", ""));
        message.setText(formStore.getString("message", ""));
        age.setChecked(formStore.getBoolean("age", false));
    }

    /**
     * Almacena los datos
     */
    @Override
    public void onPause() {
        super.onPause();
        if (submitSuccess) {
            formStore.edit().clear().commit();
        } else {
            SharedPreferences.Editor editor = formStore.edit();
            editor.putString("email", email.getText().toString());
            editor.putString("message", message.getText().toString());
            editor.putBoolean("age", age.isChecked());
            editor.commit();
        }
    }

    /**
     * Marca la operación como exitosa y la termina
     * @param v
     */
    @Override
    public void onClick(View v) {
        submitSuccess = true;
        finish();
    }
}