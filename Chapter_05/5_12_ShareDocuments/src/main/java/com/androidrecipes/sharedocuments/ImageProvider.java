package com.androidrecipes.sharedocuments;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Point;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract.Document;
import android.provider.DocumentsContract.Root;
import android.provider.DocumentsProvider;
import android.util.ArrayMap;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

public class ImageProvider extends DocumentsProvider {
    private static final String TAG = "ImageProvider";


    private static String sLastDocumentId;

    private static final String DOCID_ROOT = "root:";
    private static final String DOCID_ICONS_DIR = DOCID_ROOT + "icons:";
    private static final String DOCID_BGS_DIR = DOCID_ROOT + "backgrounds:";
    /**
     * Proyección default
     */
    private static final String[] DEFAULT_ROOT_PROJECTION = {
            Root.COLUMN_ROOT_ID, Root.COLUMN_MIME_TYPES,
            Root.COLUMN_FLAGS, Root.COLUMN_ICON, Root.COLUMN_TITLE,
            Root.COLUMN_SUMMARY, Root.COLUMN_DOCUMENT_ID,
            Root.COLUMN_AVAILABLE_BYTES
    };

    private static final String[] DEFAULT_DOCUMENT_PROJECTION = {
            Document.COLUMN_DOCUMENT_ID, Document.COLUMN_MIME_TYPE,
            Document.COLUMN_DISPLAY_NAME, Document.COLUMN_LAST_MODIFIED,
            Document.COLUMN_FLAGS, Document.COLUMN_SIZE
    };

    private ArrayMap<String, String> mIcons;
    private ArrayMap<String, String> mBackgrounds;

    /**
     * Datos por defecto
     */
    @Override
    public boolean onCreate() {
        mIcons = new ArrayMap<String, String>();
        mIcons.put("logo1.png", "John Doe");
        mIcons.put("logo2.png", "Jane Doe");
        mIcons.put("logo3.png", "Jill Doe");
        mBackgrounds = new ArrayMap<String, String>();
        mBackgrounds.put("background.jpg", "Wavy Grass");

        writeAssets(mIcons.keySet());
        writeAssets(mBackgrounds.keySet());
        return true;
    }

     /**
     * Para pasar algunos archivos al directorio de almacenamiento interno
     * @param filenames
     */
    private void writeAssets(Set<String> filenames) {
        for (String name : filenames) {
            try {
                Log.d("ImageProvider", "Writing " + name + " to storage");
                InputStream in = getContext().getAssets().open(name);
                FileOutputStream out = getContext().openFileOutput(name, Context.MODE_PRIVATE);

                int size;
                byte[] buffer = new byte[1024];
                while ((size = in.read(buffer, 0, 1024)) >= 0) {
                    out.write(buffer, 0, size);
                }
                out.flush();
                out.close();
            } catch (IOException e) {
                Log.w(TAG, e);
            }
        }
    }

    /**
     * Para crear el identificador a partir del nombre de archivo
     * @param filename
     */
    private String getIconsDocumentId(String filename) {
        return DOCID_ICONS_DIR + filename;
    }

    private String getBackgroundsDocumentId(String filename) {
        return DOCID_BGS_DIR + filename;
    }

    /**
     * Para determinar los tipos de documentos
     * @param documentId
     * @return
     */
    private boolean isRoot(String documentId) {
        return DOCID_ROOT.equals(documentId);
    }

    private boolean isIconsDir(String documentId) {
        return DOCID_ICONS_DIR.equals(documentId);
    }

    private boolean isBackgroundsDir(String documentId) {
        return DOCID_BGS_DIR.equals(documentId);
    }

    private boolean isIconDocument(String documentId) {
        return documentId.startsWith(DOCID_ICONS_DIR);
    }

    private boolean isBackgroundsDocument(String documentId) {
        return documentId.startsWith(DOCID_BGS_DIR);
    }

    /**
     * Para extraer el nombre de archivo del identificador
     * @param documentId
     */
    private String getFilename(String documentId) {
        int split = documentId.lastIndexOf(":");
        if (split < 0) {
            return "";
        }
        return documentId.substring(split + 1);
    }

    /**
     * Para determinar cuantos providers se usan
     * @param projection
     * @throws FileNotFoundException
     */
    @Override
    public Cursor queryRoots(String[] projection) throws FileNotFoundException {
        if (projection == null) {
            projection = DEFAULT_ROOT_PROJECTION;
        }
        MatrixCursor result = new MatrixCursor(projection);
         MatrixCursor.RowBuilder builder = result.newRow();

        builder.add(Root.COLUMN_ROOT_ID, "root");
        builder.add(Root.COLUMN_TITLE, "Android Recipes");
        builder.add(Root.COLUMN_SUMMARY, "Android Recipes Documents Provider");
        builder.add(Root.COLUMN_ICON, R.drawable.ic_launcher);

        builder.add(Root.COLUMN_DOCUMENT_ID, DOCID_ROOT);

        builder.add(Root.COLUMN_FLAGS,
                //Resultados del sistema local
                Root.FLAG_LOCAL_ONLY
                        | Root.FLAG_SUPPORTS_RECENTS
                        | Root.FLAG_SUPPORTS_IS_CHILD);
        builder.add(Root.COLUMN_MIME_TYPES, "image/*");
        builder.add(Root.COLUMN_AVAILABLE_BYTES, 0);

        return result;
    }

     /**
     * Determina los elementos hijo para un padre
     * @param parentDocumentId
     * @param projection
     * @param sortOrder
     * @throws FileNotFoundException
     */
    @Override
    public Cursor queryChildDocuments(String parentDocumentId, String[] projection, String sortOrder)
            throws FileNotFoundException {
        if (projection == null) {
            projection = DEFAULT_DOCUMENT_PROJECTION;
        }
        MatrixCursor result = new MatrixCursor(projection);

        if (isIconsDir(parentDocumentId)) {
            //Añade todos los archivos en la colección de icons
            try {
                for (String key : mIcons.keySet()) {
                    addImageRow(result, mIcons.get(key), getIconsDocumentId(key));
                }
            } catch (IOException e) {
                return null;
            }
        } else if (isBackgroundsDir(parentDocumentId)) {
            try {
                for (String key : mBackgrounds.keySet()) {
                    addImageRow(result, mBackgrounds.get(key), getBackgroundsDocumentId(key));
                }
            } catch (IOException e) {
                return null;
            }
        } else if (isRoot(parentDocumentId)) {
            //Añade los directorios superiores
            addIconsRow(result);
            addBackgroundsRow(result);
        }

        return result;
    }

    /**
     * Devuelve la misma información proporcionada por queryChildDocuments()
     * @param documentId
     * @param projection
     * @throws FileNotFoundException
     */
    @Override
    public Cursor queryDocument(String documentId, String[] projection)
            throws FileNotFoundException {
        if (projection == null) {
            projection = DEFAULT_DOCUMENT_PROJECTION;
        }

        MatrixCursor result = new MatrixCursor(projection);

        try {
            String filename = getFilename(documentId);
            if (isRoot(documentId)) {
                addRootRow(result);
            } else if (isIconsDir(documentId)) {
                addIconsRow(result);
            } else if (isBackgroundsDir(documentId)) {
                addBackgroundsRow(result);
            } else if (isIconDocument(documentId)) {
                addImageRow(result, mIcons.get(filename), getIconsDocumentId(filename));
            } else if (isBackgroundsDocument(documentId)) {
                addImageRow(result, mBackgrounds.get(filename), getBackgroundsDocumentId(filename));
            }
        } catch (IOException e) {
            return null;
        }

        return result;
    }


    @Override
    public Cursor queryRecentDocuments(String rootId, String[] projection)
            throws FileNotFoundException {
        if (projection == null) {
            projection = DEFAULT_DOCUMENT_PROJECTION;
        }

        MatrixCursor result = new MatrixCursor(projection);

        if (sLastDocumentId != null) {
            String filename = getFilename(sLastDocumentId);
            String recentTitle = "";
            if (isIconDocument(sLastDocumentId)) {
                recentTitle = mIcons.get(filename);
            } else if (isBackgroundsDocument(sLastDocumentId)) {
                recentTitle = mBackgrounds.get(filename);
            }

            try {
                addImageRow(result, recentTitle, sLastDocumentId);
            } catch (IOException e) {
                Log.w(TAG, e);
            }
        }
        Log.d(TAG, "Recents: " + result.getCount());
             return result;
    }


    @Override
    public boolean isChildDocument(String parentDocumentId, String documentId) {
        if (isRoot(parentDocumentId)) {

            return isIconsDir(documentId)
                    || isBackgroundsDir(documentId);
        }

        if (isIconsDir(parentDocumentId)) {

            return isIconDocument(documentId);
        }

        if (isBackgroundsDir(parentDocumentId)) {

            return isBackgroundsDocument(documentId);
        }

        return false;
    }


    private void addRootRow(MatrixCursor cursor) {
        addDirRow(cursor, DOCID_ROOT, "Root");
    }

    private void addIconsRow(MatrixCursor cursor) {
        addDirRow(cursor, DOCID_ICONS_DIR, "Icons");
    }

    private void addBackgroundsRow(MatrixCursor cursor) {
        addDirRow(cursor, DOCID_BGS_DIR, "Backgrounds");
    }

    /**
     * Crea un subdirectorio
     * @param cursor
     * @param documentId
     * @param name
     */
    private void addDirRow(MatrixCursor cursor, String documentId, String name) {
        final MatrixCursor.RowBuilder row = cursor.newRow();

        row.add(Document.COLUMN_DOCUMENT_ID, documentId);
        row.add(Document.COLUMN_DISPLAY_NAME, name);
        row.add(Document.COLUMN_SIZE, 0);
        row.add(Document.COLUMN_MIME_TYPE, Document.MIME_TYPE_DIR);

        long installed;
        try {
            installed = getContext().getPackageManager()
                    .getPackageInfo(getContext().getPackageName(), 0)
                    .firstInstallTime;
        } catch (NameNotFoundException e) {
            installed = 0;
        }
        row.add(Document.COLUMN_LAST_MODIFIED, installed);
        row.add(Document.COLUMN_FLAGS, 0);
    }


    /**
     * Guarda una imagen específica en el cursor proporcionado
     * @param cursor
     * @param title
     * @param documentId
     * @throws IOException
     */
    private void addImageRow(MatrixCursor cursor, String title, String documentId) throws IOException {
        final MatrixCursor.RowBuilder row = cursor.newRow();

        String filename = getFilename(documentId);
        AssetFileDescriptor afd = getContext().getAssets().openFd(filename);

        row.add(Document.COLUMN_DOCUMENT_ID, documentId);
        row.add(Document.COLUMN_DISPLAY_NAME, title);
        row.add(Document.COLUMN_SIZE, afd.getLength());
        row.add(Document.COLUMN_MIME_TYPE, "image/*");

        long installed;
        try {
            installed = getContext().getPackageManager()
                    .getPackageInfo(getContext().getPackageName(), 0)
                    .firstInstallTime;
        } catch (NameNotFoundException e) {
            installed = 0;
        }
        row.add(Document.COLUMN_LAST_MODIFIED, installed);
        row.add(Document.COLUMN_FLAGS, Document.FLAG_SUPPORTS_THUMBNAIL);
    }


    /**
     * Devuelve una referencia a un asset de imagen que el framework usará
     * @param documentId
     * @param sizeHint
     * @param signal
     * @throws FileNotFoundException
     */
    @Override
    public AssetFileDescriptor openDocumentThumbnail(String documentId, Point sizeHint, CancellationSignal signal)
            throws FileNotFoundException {
        //Cara la vista previa
        String filename = getFilename(documentId);
        //Crea un archivo de referencia
        final File file = new File(getContext().getFilesDir(), filename);
        final ParcelFileDescriptor pfd =
                ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
        return new AssetFileDescriptor(pfd, 0, AssetFileDescriptor.UNKNOWN_LENGTH);
    }

     /**
     * Devuelve un archivo de descripción al documento referenciado por medio del identificador proporcionado
     * @param documentId
     * @param mode
     * @param signal
     * @throws FileNotFoundException
     */
    @Override
    public ParcelFileDescriptor openDocument(String documentId, String mode, CancellationSignal signal)
            throws FileNotFoundException {
        //Cargamos el documento desde sus assets
        try {
            String filename = getFilename(documentId);
            //Crea un archivo de referencia de la imagen
            final File file = new File(getContext().getFilesDir(), filename);
            final ParcelFileDescriptor pfd =
                    ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);

            //Guarda el últmo documento seleccionado
            sLastDocumentId = documentId;

            return pfd;
        } catch (IOException e) {
            Log.w(TAG, e);
            return null;
        }
    }
}
