1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.androidrecipes.sharedocuments"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="19"
8-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml
9        android:targetSdkVersion="29" />
9-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml
10
11    <application
11-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:6:5-34:19
12        android:allowBackup="true"
12-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:7:9-35
13        android:debuggable="true"
14        android:icon="@drawable/ic_launcher"
14-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:8:9-45
15        android:label="@string/app_name"
15-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:9:9-41
16        android:testOnly="true"
17        android:theme="@style/AppTheme" >
17-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:10:9-40
18        <activity android:name="com.androidrecipes.sharedocuments.DocumentActivity" >
18-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:11:9-16:20
18-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:11:19-51
19            <intent-filter>
19-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:12:13-15:29
20                <action android:name="android.intent.action.MAIN" />
20-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:13:17-68
20-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:13:25-66
21
22                <category android:name="android.intent.category.LAUNCHER" />
22-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:14:17-76
22-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:14:27-74
23            </intent-filter>
24        </activity>
25
26        <!--
27          Must use the MANAGE_DOCUMENTS permission to protect access to system only.
28          Add grantUriPermissions to allow provider to give client permission to access
29          single files.
30        -->
31        <provider
31-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:23:9-33:20
32            android:name="com.androidrecipes.sharedocuments.ImageProvider"
32-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:24:13-75
33            android:authorities="com.androidrecipes.sharedocuments.images"
33-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:25:13-75
34            android:exported="true"
34-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:27:13-36
35            android:grantUriPermissions="true"
35-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:26:13-47
36            android:permission="android.permission.MANAGE_DOCUMENTS" >
36-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:28:13-69
37
38            <!-- Unique filter the system will use to find published providers -->
39            <intent-filter>
39-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:30:13-32:29
40                <action android:name="android.content.action.DOCUMENTS_PROVIDER" />
40-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:31:17-83
40-->C:\Users\Dell\AndroidStudioProjects\Chapter05_Source\5_12_ShareDocuments\src\main\AndroidManifest.xml:31:25-81
41            </intent-filter>
42        </provider>
43    </application>
44
45</manifest>
