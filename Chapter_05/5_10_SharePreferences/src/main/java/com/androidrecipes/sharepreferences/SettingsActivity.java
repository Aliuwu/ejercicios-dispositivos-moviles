package com.androidrecipes.sharepreferences;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

/**
 * Problema: Se desea que la aplicación proporcione los valores de configuración almacenados en SharedPreferences a otras aplicaciones
 * Solución: Crearemos un ContentProvider y MatrixCursor
 */

public class SettingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Carga las preferencias por defecto
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        addPreferencesFromResource(R.xml.preferences);
    }
}
