package com.androidrecipes.preferencesnew;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
/**
 * Problema: Se necesita crear una manera simple de almacenar, cambiar y mostrar las configuraciones del usuario
 * Solucion: Usaremos PreferenceActivity y una archivo XML de la jerarquía de Preference
 */
public class MainActivity extends Activity {

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(android.R.id.content, new HomeFragment());
        ft.commit();
    }

}
