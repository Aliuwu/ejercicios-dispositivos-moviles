package com.androidrecipes.preferencesnew;

import android.os.Bundle;
import android.preference.PreferenceFragment;

public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Carga los datos de preferencia desde XML
        addPreferencesFromResource(R.xml.settings);
    }
}
