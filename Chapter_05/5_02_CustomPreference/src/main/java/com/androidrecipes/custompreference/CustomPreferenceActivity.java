package com.androidrecipes.custompreference;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Problema: Se necesita añadir una interfaz de usuario más flexible
 * Solución: Usaremos PreferenceActivity y SharedPreferences
 */
public class CustomPreferenceActivity extends PreferenceActivity {

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }
}
