package com.androidrecipes.custompreference;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.preference.DialogPreference;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;

public class ColorPreference extends DialogPreference {

    private static final int DEFAULT_COLOR = Color.WHITE;
    private int mCurrentColor;
    private SeekBar mRedLevel, mGreenLevel, mBlueLevel;

    public ColorPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    /**
     * Cuadro de diálogo cuando se selecciona la preferencia
     * @param builder
     */
    @Override
    protected void onPrepareDialogBuilder(Builder builder) {
        //Crea el contenido de la vista
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.preference_color, null);
        mRedLevel = (SeekBar) rootView.findViewById(R.id.selector_red);
        mGreenLevel = (SeekBar) rootView.findViewById(R.id.selector_green);
        mBlueLevel = (SeekBar) rootView.findViewById(R.id.selector_blue);

        mRedLevel.setProgress(Color.red(mCurrentColor));
        mGreenLevel.setProgress(Color.green(mCurrentColor));
        mBlueLevel.setProgress(Color.blue(mCurrentColor));

        builder.setView(rootView);
        super.onPrepareDialogBuilder(builder);
    }

     /**
     * Cuando se termina la selección de la preferencia
     * @param positiveResult
     */
    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            //Obtiene el color elegido
            int color = Color.rgb(
                    mRedLevel.getProgress(),
                    mGreenLevel.getProgress(),
                    mBlueLevel.getProgress());
            setCurrentValue(color);
        }
    }

    /**
     * Obtiene el valor por defecto de la preferencia
     * @param a
     * @param index
     */
    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        //Devuele el valor por defecto desde XML como un color int
        ColorStateList value = a.getColorStateList(index);
        if (value == null) {
            return DEFAULT_COLOR;
        }
        return value.getDefaultColor();
    }

     /**
     * Asigna el valor inicial de la preferencia
     * @param restorePersistedValue
     * @param defaultValue
     */
    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        setCurrentValue(restorePersistedValue ? getPersistedInt(DEFAULT_COLOR) : (Integer) defaultValue);
    }

    /**
     * Devuelve un resumen basado en las configuraciones actuales
     */
    @Override
    public CharSequence getSummary() {
        int color = getPersistedInt(DEFAULT_COLOR);
        String content = String.format("Current Value is 0x%02X%02X%02X",
                Color.red(color), Color.green(color), Color.blue(color));
        Spannable summary = new SpannableString(content);
        summary.setSpan(new ForegroundColorSpan(color), 0, summary.length(), 0);
        return summary;
    }

    /**
     * Asigna el valor actual
     * @param value
     */
    private void setCurrentValue(int value) {
        //Actualiza el último valor
        mCurrentColor = value;

        //Guarda el nuevo valor
        persistInt(value);
        //Notifica a los listeners de preferencia
        notifyDependencyChange(shouldDisableDependents());
        notifyChanged();
    }

}
