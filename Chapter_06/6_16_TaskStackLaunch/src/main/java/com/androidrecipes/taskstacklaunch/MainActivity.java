package com.androidrecipes.taskstacklaunch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity implements View.OnClickListener {

    public static final String ACTION_NEW_ARRIVAL =
            "com.examples.taskstack.ACTION_NEW_ARRIVAL";

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //Listeners de los botones
        findViewById(R.id.button_nephew).setOnClickListener(this);
        findViewById(R.id.button_niece).setOnClickListener(this);
        findViewById(R.id.button_twins).setOnClickListener(this);
    }

    /**
     * Menú de opciones
     * @param v
     */
    @Override
    public void onClick(View v) {
        String newArrival;
        switch (v.getId()) {
            case R.id.button_nephew:
                newArrival = "Baby Nephew";
                break;
            case R.id.button_niece:
                newArrival = "Baby Niece";
                break;
            case R.id.button_twins:
                newArrival = "Twin Nieces!";
                break;
            default:
                return;
        }

        /**
         * Crea un intent para iniciar la actividad
         */
        Intent intent = new Intent(ACTION_NEW_ARRIVAL);
        intent.putExtra(Intent.EXTRA_TEXT, newArrival);
        startActivity(intent);
    }
}
