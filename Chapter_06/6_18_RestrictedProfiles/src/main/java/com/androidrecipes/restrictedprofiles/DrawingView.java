package com.androidrecipes.restrictedprofiles;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class DrawingView extends View {

    private Paint mFingerPaint;
    private Path mPath;

    /**
     * Constructor 1
     * @param context
     */
    public DrawingView(Context context) {
        super(context);
        init();
    }

    /**
     * Constructor 2
     * @param context
     * @param attrs
     */
    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawingView(Context context, AttributeSet attrs,
                       int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        //configura la brocha
        mFingerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mFingerPaint.setStyle(Style.STROKE);
        mFingerPaint.setStrokeCap(Cap.ROUND);
        mFingerPaint.setStrokeJoin(Join.ROUND);
        //Ancho del trazo por defecto
        mFingerPaint.setStrokeWidth(8f);
    }

    /**
     * Color de la pintura
     * @param color
     */
    public void setPaintColor(int color) {
        mFingerPaint.setColor(color);
    }

    /**
     * Ancho del trazo
     * @param width
     */
    public void setStrokeWidth(float width) {
        mFingerPaint.setStrokeWidth(width);
    }

    /**
     * Color del lienzo
     * @param color
     */
    public void setCanvasColor(int color) {
        setBackgroundColor(color);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                mPath = new Path();
                mPath.moveTo(event.getX(), event.getY());
                //Redibujar
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                //Añade los puntos entre eventos
                for (int i = 0; i < event.getHistorySize(); i++) {
                    mPath.lineTo(event.getHistoricalX(i),
                            event.getHistoricalY(i));
                }
                //Redibujar
                invalidate();
                break;
            default:
                break;
        }
        return true;
    }

    /**
     * Dibuja los componentes
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        //Dibuja el fondo
        super.onDraw(canvas);
        //Dibuja el trazo de la brocha
        if (mPath != null) {
            canvas.drawPath(mPath, mFingerPaint);
        }
    }
}
