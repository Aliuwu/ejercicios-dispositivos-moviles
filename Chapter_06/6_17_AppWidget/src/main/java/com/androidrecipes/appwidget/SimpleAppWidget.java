package com.androidrecipes.appwidget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;

public class SimpleAppWidget extends AppWidgetProvider {

      /**
     * Actualiza los widgets creados en el provider
     * @param context
     * @param appWidgetManager
     * @param appWidgetIds
     */
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        //Inicia el proceso en segundo plano
        context.startService(new Intent(context, RandomService.class));
    }
}
