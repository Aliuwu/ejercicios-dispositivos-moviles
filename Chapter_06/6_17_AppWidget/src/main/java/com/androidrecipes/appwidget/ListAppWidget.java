package com.androidrecipes.appwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.widget.RemoteViews;

public class ListAppWidget extends AppWidgetProvider {

    /**
     * Actualiza los widgets creados por el provider
     * @param context
     * @param appWidgetManager
     * @param appWidgetIds
     */
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int i = 0; i < appWidgetIds.length; i++) {
            Intent intent = new Intent(context, ListWidgetService.class);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);
            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.list_widget_layout);
            //Asigna el título basado en la configuración del widget
            SharedPreferences prefs = context.getSharedPreferences(String.valueOf(appWidgetIds[i]), Context.MODE_PRIVATE);
            String mode = prefs.getString(ListWidgetService.KEY_MODE, ListWidgetService.MODE_IMAGE);
            if (ListWidgetService.MODE_VIDEO.equals(mode)) {
                views.setTextViewText(R.id.text_title, "Video Collection");
            } else {
                views.setTextViewText(R.id.text_title, "Image Collection");
            }

            views.setRemoteAdapter(appWidgetIds[i], R.id.list, intent);

            views.setEmptyView(R.id.list, R.id.list_empty);

            //Intent para los itemas de cada lista
            Intent viewIntent = new Intent(Intent.ACTION_VIEW);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, viewIntent, 0);
            views.setPendingIntentTemplate(R.id.list, pendingIntent);

            appWidgetManager.updateAppWidget(appWidgetIds[i], views);
        }
    }

    /**
     * El primer widget se añade al provider
     */
    @Override
    public void onEnabled(Context context) {
        //Empieza el servicio
        context.startService(new Intent(context, MediaService.class));
    }

    /**
     * Todos los widgets se eliminan del provider
     */
    @Override
    public void onDisabled(Context context) {
        //Termina el servicio
        context.stopService(new Intent(context, MediaService.class));
    }

      /**
     * Uno o más widgets son eliminados del provider
     * @param context
     * @param appWidgetIds
     */
    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        for (int i = 0; i < appWidgetIds.length; i++) {
            context.getSharedPreferences(String.valueOf(appWidgetIds[i]), Context.MODE_PRIVATE)
                    .edit()
                    .clear()
                    .commit();
        }

    }
}
