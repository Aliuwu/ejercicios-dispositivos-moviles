package com.androidrecipes.appwidget;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;

public class MediaService extends Service {

    private ContentObserver mMediaStoreObserver;

    /**
     * Inicializa la actividad
     */
    @Override
    public void onCreate() {
        super.onCreate();
        //Crea un registro nuevo cuando el servicio inicia
        mMediaStoreObserver = new ContentObserver(new Handler()) {
            @Override
            public void onChange(boolean selfChange) {
                //Actualiza todos los widgets
                AppWidgetManager manager = AppWidgetManager.getInstance(MediaService.this);
                ComponentName provider = new ComponentName(MediaService.this, ListAppWidget.class);
                int[] appWidgetIds = manager.getAppWidgetIds(provider);
                manager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.list);
            }
        };
        //Registro para imágenes y video
        getContentResolver().registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, true, mMediaStoreObserver);
        getContentResolver().registerContentObserver(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, true, mMediaStoreObserver);
    }

    /**
     * Elimina el registro
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        getContentResolver().unregisterContentObserver(mMediaStoreObserver);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
