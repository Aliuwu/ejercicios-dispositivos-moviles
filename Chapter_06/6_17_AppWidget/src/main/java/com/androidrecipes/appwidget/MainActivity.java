package com.androidrecipes.appwidget;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * Problema: La aplicación proporciona información a la que los usuarios necesitan acceder rápido y regularmente.
 * Solución: Usaremos un AppWidget que los usuarios pueden escoger instalar en la pantalla de incio como parte de la aplicación.
 */
public class MainActivity extends Activity {

    private TextView mCurrentNumber;

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mCurrentNumber = (TextView) findViewById(R.id.text_number);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateNumberView();
        //Registra un receptor para recibir actualizaciones cuando finalice el servicio
        IntentFilter filter = new IntentFilter(RandomService.ACTION_RANDOM_NUMBER);
        registerReceiver(mReceiver, filter);
    }

    /**
     * Elimina el registro del receptor
     */
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    /**
     * Llama al servicio para actualizar los datos
     * @param v
     */
    public void onRandomClick(View v) {
        startService(new Intent(this, RandomService.class));
    }

    /**
     * Actualiza la vista con el último número
     */
    private void updateNumberView() {
        mCurrentNumber.setText(String.valueOf(RandomService.getRandomNumber()));
    }

    /**
     * Actualiza la vista con el nuevo número
     */
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateNumberView();
        }
    };
}
