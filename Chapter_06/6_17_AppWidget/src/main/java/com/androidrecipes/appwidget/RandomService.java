package com.androidrecipes.appwidget;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.IBinder;
import android.widget.RemoteViews;

public class RandomService extends Service {
    public static final String ACTION_RANDOM_NUMBER = "com.examples.appwidget.ACTION_RANDOM_NUMBER";

    private static int sRandomNumber;

    public static int getRandomNumber() {
        return sRandomNumber;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Actualiza los datos random
        sRandomNumber = (int) (Math.random() * 100);

        //Crea la vista de AppWidget
        RemoteViews views = new RemoteViews(getPackageName(), R.layout.simple_widget_layout);
        views.setTextViewText(R.id.text_number, String.valueOf(sRandomNumber));

        //Asigna un Intent para el botón de refrescar
        PendingIntent refreshIntent = PendingIntent.getService(this, 0,
                new Intent(this, RandomService.class), 0);
        views.setOnClickPendingIntent(R.id.button_refresh, refreshIntent);

        //Asigna un Intent para abrir el widget con un toque
        PendingIntent appIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);
        views.setOnClickPendingIntent(R.id.container, appIntent);

        //Actualiza el widget
        AppWidgetManager manager = AppWidgetManager.getInstance(this);
        ComponentName widget = new ComponentName(this, SimpleAppWidget.class);
        manager.updateAppWidget(widget, views);

        //Notifica a los listeners
        Intent broadcast = new Intent(ACTION_RANDOM_NUMBER);
        sendBroadcast(broadcast);

        stopSelf();
        return START_NOT_STICKY;
    }

       @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
