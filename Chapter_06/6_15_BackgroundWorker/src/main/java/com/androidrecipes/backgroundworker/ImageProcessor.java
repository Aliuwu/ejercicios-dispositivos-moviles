package com.androidrecipes.backgroundworker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

public class ImageProcessor extends HandlerThread implements Handler.Callback {
    public static final int MSG_SCALE = 100;
    public static final int MSG_CROP = 101;

    private Context mContext;
    private Handler mReceiver, mCallback;

    /**
     * Constructor 1
     * @param context
     */
    public ImageProcessor(Context context) {
        this(context, null);
    }

    /**
     * Constructor 2
     * @param context
     * @param callback
     */
    public ImageProcessor(Context context, Handler callback) {
        super("AndroidRecipesWorker");
        mCallback = callback;
        mContext = context.getApplicationContext();
    }

    @Override
    protected void onLooperPrepared() {
        mReceiver = new Handler(getLooper(), this);
    }

    /**
     * Recupera los argumentos para el mensaje
     * @param msg
     */
    @Override
    public boolean handleMessage(Message msg) {
        Bitmap source, result;
        int scale = msg.arg1;
        switch (msg.what) {
            case MSG_SCALE:
                source = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.ic_launcher);
                //Crea una nueva imagen escalada
                result = Bitmap.createScaledBitmap(source,
                        source.getWidth() * scale, source.getHeight() * scale, true);
                break;
            case MSG_CROP:
                source = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.ic_launcher);
                int newWidth = source.getWidth() / scale;
                //Crea una imagen recortada horizontalmente
                result = Bitmap.createBitmap(source,
                        (source.getWidth() - newWidth) / 2, 0,
                        newWidth, source.getHeight());
                break;
            default:
                throw new IllegalArgumentException("Unknown Worker Request");
        }

        // Devuelve la imagen al hilo principal
        if (mCallback != null) {
            mCallback.sendMessage(Message.obtain(null, 0, result));
        }
        return true;
    }

    /**
     * Quitar el callback handler
     * @param callback
     */
    public void setCallback(Handler callback) {
        mCallback = callback;
    }


    /**
     *Métodos para poner el trabajo en espera
     * @param scale
     */
    // Escala el ícono a valor especificado
    public void scaleIcon(int scale) {
        Message msg = Message.obtain(null, MSG_SCALE, scale, 0, null);
        mReceiver.sendMessage(msg);
    }

    //Recorta el ícono  en el centro y escala el resultado al valor específico
    public void cropIcon(int scale) {
        Message msg = Message.obtain(null, MSG_CROP, scale, 0, null);
        mReceiver.sendMessage(msg);
    }
}
