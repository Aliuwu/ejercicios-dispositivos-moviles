package com.androidrecipes.backgroundworker;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;

/**
 * Problema: Se necesita crear una tarea en segundo plano que se ejecute por un largo periodo de tiempo que espere a que una tarea se ejecute y que pueda ser terminado cuando ya no se lo necesite
 * Solución: Usaremos HandlerThread para crear la tarea en segundo plano
 */
public class WorkerActivity extends Activity implements Handler.Callback {

    private ImageProcessor mWorker;
    private Handler mResponseHandler;

    private ImageView mResultView;

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mResultView = (ImageView) findViewById(R.id.image_result);
        //Handler para manejar las actividades en segundo plano
        mResponseHandler = new Handler(this);
    }

    /**
     * Empezar una nueva tarea
     */
    @Override
    protected void onResume() {
        super.onResume();
        mWorker = new ImageProcessor(this, mResponseHandler);
        mWorker.start();
    }

    /**
     * Finalizar la tarea
     */
    @Override
    protected void onPause() {
        super.onPause();
        mWorker.setCallback(null);
        mWorker.quit();
        mWorker = null;
    }

     /**
     * Resultados de las tareas en segundo plano
     * @param msg
     * @return
     */
    @Override
    public boolean handleMessage(Message msg) {
        Bitmap result = (Bitmap) msg.obj;
        mResultView.setImageBitmap(result);
        return true;
    }

    /**
     * Método para activar las tareas en segundo plano
     * @param v
     */
    public void onScaleClick(View v) {
        for (int i = 1; i < 10; i++) {
            mWorker.scaleIcon(i);
        }
    }

    public void onCropClick(View v) {
        for (int i = 1; i < 10; i++) {
            mWorker.cropIcon(i);
        }
    }
}
