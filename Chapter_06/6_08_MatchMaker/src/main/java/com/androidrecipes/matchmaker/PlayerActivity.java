package com.androidrecipes.matchmaker;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

/**
 * Problema: se desea construir una interfaz para que otras aplicaciones en el dispositivo puedan ejecutar la aplicación.
 * Solución: Crearemos un IntentFilter en la actividad o servicio que deseamos construir
 */
public class PlayerActivity extends Activity {

    public static final String ACTION_PLAY = "com.examples.myplayer.PLAY";

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        //Inspecciona el intent
        Intent incoming = getIntent();
        //Obtiene la fuente del video
        Uri videoUri = incoming.getData();
        //Obtiene el título extra si existe
        String title;
        if (incoming.hasExtra(Intent.EXTRA_TITLE)) {
            title = incoming.getStringExtra(Intent.EXTRA_TITLE);
        } else {
            title = "";
        }

        //Se reproduce el video
        Log.i("PLAYER", "Launched");
    }

}