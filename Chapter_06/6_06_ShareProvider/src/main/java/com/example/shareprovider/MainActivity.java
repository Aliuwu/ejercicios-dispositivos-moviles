package com.example.shareprovider;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ShareActionProvider;

/**
 * Problema: La aplicación requiere una función específica que una de las aplicaciones del sistema este programada para hacer
 * Solución: Usaremos un intent de manera implícita para especificarle al sistema cual es la aplicación de interés
 */
public class MainActivity extends Activity {

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    /**
     * Menú con los items que serán compartidos
     * @param menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options, menu);


        MenuItem item = menu.findItem(R.id.menu_share);
        ShareActionProvider provider = (ShareActionProvider) item.getActionProvider();
        //Crea el intent para buscar la aplicación del sistema
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, "Text Sharing Update");
        provider.setShareIntent(intent);

        return true;
    }
}
