package com.androidrecipes.contacts;

import android.app.AlertDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;

/**
 * Problema: La aplicación necesita interactuar directamente con el ContentProvider mostrado por Android para editar, ver o eliminar información de los contactos del usuario
 * Solución: Usaremos la interfaz mostrada por ContactsContract para acceder a los datos
 */
public class ContactsActivity extends FragmentActivity {

    private static final int ROOT_ID = 100;

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout rootView = new FrameLayout(this);
        rootView.setId(ROOT_ID);

        setContentView(rootView);

        //Crea y añade una nueva lista de fragments
        getSupportFragmentManager().beginTransaction()
                .add(ROOT_ID, ContactsFragment.newInstance())
                .commit();
    }

    public static class ContactsFragment extends ListFragment
            implements AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

        public static ContactsFragment newInstance() {
            return new ContactsFragment();
        }

        private SimpleCursorAdapter mAdapter;

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            // Muestra todos los contactos en un ListView
            mAdapter = new SimpleCursorAdapter(getActivity(),
                    android.R.layout.simple_list_item_1, null,
                    new String[]{ContactsContract.Contacts.DISPLAY_NAME},
                    new int[]{android.R.id.text1},
                    0);
            setListAdapter(mAdapter);
            getListView().setOnItemClickListener(this);

            getLoaderManager().initLoader(0, null, this);
        }

        /**
         * Devuelve los contactos ordenados por nombre
         * @param id
         * @param args
         */
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            String[] projection = new String[]{
                    ContactsContract.Contacts._ID,
                    ContactsContract.Contacts.DISPLAY_NAME
            };

            return new CursorLoader(getActivity(),
                    ContactsContract.Contacts.CONTENT_URI,
                    projection, null, null,
                    ContactsContract.Contacts.DISPLAY_NAME);
        }


        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            mAdapter.swapCursor(data);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            mAdapter.swapCursor(null);
        }

        /**
         * Muestra la información de los contactos
         * @param parent
         * @param v
         * @param position
         * @param id
         */
        @Override
        public void onItemClick(AdapterView<?> parent, View v,
                                int position, long id) {
            final Cursor contacts = mAdapter.getCursor();
            if (contacts.moveToPosition(position)) {
                int selectedId = contacts.getInt(0);
                // Email
                Cursor email = getActivity().getContentResolver()
                        .query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                                new String[]{ContactsContract.CommonDataKinds.Email.DATA},
                                ContactsContract.Data.CONTACT_ID
                                        + " = " + selectedId,
                                null, null);
                // Teléfono
                Cursor phone = getActivity().getContentResolver()
                        .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
                                ContactsContract.Data.CONTACT_ID
                                        + " = " + selectedId,
                                null, null);
                //Dirección
                Cursor address = getActivity().getContentResolver()
                        .query(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI,
                                new String[]{ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS},
                                ContactsContract.Data.CONTACT_ID
                                        + " = " + selectedId,
                                null, null);

                //Caja de diálogo
                StringBuilder sb = new StringBuilder();
                sb.append(email.getCount() + " Emails\n");
                if (email.moveToFirst()) {
                    do {
                        sb.append("Email: " + email.getString(0));
                        sb.append('\n');
                    } while (email.moveToNext());
                    sb.append('\n');
                }
                sb.append(phone.getCount() + " Phone Numbers\n");
                if (phone.moveToFirst()) {
                    do {
                        sb.append("Phone: " + phone.getString(0));
                        sb.append('\n');
                    } while (phone.moveToNext());
                    sb.append('\n');
                }
                sb.append(address.getCount() + " Addresses\n");
                if (address.moveToFirst()) {
                    do {
                        sb.append("Address:\n"
                                + address.getString(0));
                    } while (address.moveToNext());
                    sb.append('\n');
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(contacts.getString(1)); // Display name
                builder.setMessage(sb.toString());
                builder.setPositiveButton("OK", null);
                builder.create().show();

                /**
                 * Cierra la información
                 */
                email.close();
                phone.close();
                address.close();
            }
        }
    }
}
