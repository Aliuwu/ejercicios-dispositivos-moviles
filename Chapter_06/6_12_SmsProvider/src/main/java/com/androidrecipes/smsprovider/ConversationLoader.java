package com.androidrecipes.smsprovider;

import android.content.AsyncTaskLoader;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.Telephony.Mms;
import android.provider.Telephony.MmsSms;
import android.provider.Telephony.Sms;
import android.provider.Telephony.Sms.Conversations;
import android.telephony.TelephonyManager;

import java.util.List;

public class ConversationLoader extends AsyncTaskLoader<List<MessageItem>> {

    public static final String[] PROJECTION = new String[]{
            //Determina si es SMS o MMS
            MmsSms.TYPE_DISCRIMINATOR_COLUMN,
            //identificador del item
            BaseColumns._ID,
            //identificador de la conversación
            Conversations.THREAD_ID,
            //Valores de la fecha
            Sms.DATE,
            Sms.DATE_SENT,
            // Para SMS
            Sms.ADDRESS,
            Sms.BODY,
            Sms.TYPE,
            // Para MMS
            Mms.SUBJECT,
            Mms.MESSAGE_BOX
    };

    //Identificador del thread de la conversación que está siendo cargada
    private long mThreadId;
    //el número del dispositivo
    private String mDeviceNumber;

    public ConversationLoader(Context context) {
        this(context, -1);
    }

    /**
     * Obtiene el número del dispositivo
     * @param context
     * @param threadId
     */
    public ConversationLoader(Context context, long threadId) {
        super(context);
        mThreadId = threadId;
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        mDeviceNumber = manager.getLine1Number();
    }

    /**
     * Recarga en cada solicitud
     */
    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    /**
     * Para cargar las conversaciones
     */
    @Override
    public List<MessageItem> loadInBackground() {
        Uri uri;
        String[] projection;
        if (mThreadId < 0) {
            //Todas las conversaciones
            uri = MmsSms.CONTENT_CONVERSATIONS_URI;
            projection = null;
        } else {
            //Solo las requeridas
            uri = ContentUris.withAppendedId(MmsSms.CONTENT_CONVERSATIONS_URI, mThreadId);
            projection = PROJECTION;
        }

        Cursor cursor = getContext().getContentResolver().query(
                uri,
                projection,
                null,
                null,
                null);

        return MessageItem.parseMessages(getContext(), cursor, mDeviceNumber);
    }
}
