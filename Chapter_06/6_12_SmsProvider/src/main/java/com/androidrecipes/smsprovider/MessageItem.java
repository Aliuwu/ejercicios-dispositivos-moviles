package com.androidrecipes.smsprovider;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.Telephony.Mms;
import android.provider.Telephony.MmsSms;
import android.provider.Telephony.Sms;
import android.provider.Telephony.Sms.Conversations;
import android.provider.Telephony.TextBasedSmsColumns;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class MessageItem {
    private static final String TYPE_SMS = "sms";
    private static final String TYPE_MMS = "mms";

    static final String[] MMS_PROJECTION = new String[]{
            BaseColumns._ID,
            //Tipo de contenido
            Mms.Part.CONTENT_TYPE,
            //Contenido de un texto sin formato
            Mms.Part.TEXT,
            //Ruta al contenido binario de una parte que no es de texto
            Mms.Part._DATA
    };

    //Identificador del mensaje
    public long id;
    //Identificadro de la conversación
    public long thread_id;
    // Dirección del mesaje
    public String address;
    //Cuerpo del mensaje
    public String body;
    //Para verificar si el mensaje fue recibido
    public boolean incoming;

    public Uri attachment;


    /**
     * construye una lista de mensajes
     * @param context
     * @param cursor
     * @param myNumber
     */
    public static List<MessageItem> parseMessages(Context context, Cursor cursor, String myNumber) {
        List<MessageItem> messages = new ArrayList<MessageItem>();
        if (!cursor.moveToFirst()) {
            return messages;
        }
        //Analizar cada mensaje según los identificadores de tipo
        do {
            String type = getMessageType(cursor);
            if (TYPE_SMS.equals(type)) {
                MessageItem item = parseSmsMessage(cursor);
                messages.add(item);
            } else if (TYPE_MMS.equals(type)) {
                MessageItem item = parseMmsMessage(context, cursor, myNumber);
                messages.add(item);
            } else {
                Log.w("TelephonyProvider", "Unknown Message Type");
            }
        } while (cursor.moveToNext());
        cursor.close();

        return messages;
    }

    /**
     * Lee el tipo de mensaje
     * @param cursor
     */
    private static String getMessageType(Cursor cursor) {
        int typeIndex = cursor.getColumnIndex(MmsSms.TYPE_DISCRIMINATOR_COLUMN);
        if (typeIndex < 0) {
            String cType = cursor.getString(cursor.getColumnIndex(Mms.CONTENT_TYPE));
            if (cType != null) {
                return TYPE_MMS;
            } else {
                return TYPE_SMS;
            }
        } else {
            return cursor.getString(typeIndex);
        }
    }

    /**
     * Mensaje SMS
     * @param data
     */
    private static MessageItem parseSmsMessage(Cursor data) {
        MessageItem item = new MessageItem();
        item.id = data.getLong(data.getColumnIndexOrThrow(BaseColumns._ID));
        item.thread_id = data.getLong(data.getColumnIndexOrThrow(Conversations.THREAD_ID));

        item.address = data.getString(data.getColumnIndexOrThrow(Sms.ADDRESS));
        item.body = data.getString(data.getColumnIndexOrThrow(Sms.BODY));
        item.incoming = isIncomingMessage(data, true);
        return item;
    }

    /**
     * Mensaje MMS
     * @param context
     * @param data
     * @param myNumber
     */
    private static MessageItem parseMmsMessage(Context context, Cursor data, String myNumber) {
        MessageItem item = new MessageItem();
        item.id = data.getLong(data.getColumnIndexOrThrow(BaseColumns._ID));
        item.thread_id = data.getLong(data.getColumnIndexOrThrow(Conversations.THREAD_ID));

        item.incoming = isIncomingMessage(data, false);

        long _id = data.getLong(data.getColumnIndexOrThrow(BaseColumns._ID));

        //Consulta la dirección de la información para el mensaje
        Uri addressUri = Uri.withAppendedPath(Mms.CONTENT_URI, _id + "/addr");
        Cursor addr = context.getContentResolver().query(
                addressUri,
                null,
                null,
                null,
                null);
        HashSet<String> recipients = new HashSet<String>();
        while (addr.moveToNext()) {
            String address = addr.getString(addr.getColumnIndex(Mms.Addr.ADDRESS));
            if (myNumber == null || !address.contains(myNumber)) {
                recipients.add(address);
            }
        }
        item.address = TextUtils.join(", ", recipients);
        addr.close();

        //Consulta todas las partes MMS asociadas con el mensaje
        Uri messageUri = Uri.withAppendedPath(Mms.CONTENT_URI, _id + "/part");
        Cursor inner = context.getContentResolver().query(
                messageUri,
                MMS_PROJECTION,
                Mms.Part.MSG_ID + " = ?",
                new String[]{String.valueOf(data.getLong(data.getColumnIndexOrThrow(Mms._ID)))},
                null);

        while (inner.moveToNext()) {
            String contentType = inner.getString(inner.getColumnIndexOrThrow(Mms.Part.CONTENT_TYPE));
            if (contentType == null) {
                continue;
            } else if (contentType.matches("image/.*")) {
                long partId = inner.getLong(inner.getColumnIndexOrThrow(BaseColumns._ID));
                item.attachment = Uri.withAppendedPath(Mms.CONTENT_URI, "part/" + partId);
            } else if (contentType.matches("text/.*")) {
                item.body = inner.getString(inner.getColumnIndexOrThrow(Mms.Part.TEXT));
            }
        }

        inner.close();
        return item;
    }

    /**
     * Verifica si el mensaje es de entrada o salida
     * @param cursor
     * @param isSms
     */
    private static boolean isIncomingMessage(Cursor cursor, boolean isSms) {
        int boxId;
        if (isSms) {
            boxId = cursor.getInt(cursor.getColumnIndexOrThrow(Sms.TYPE));
            return (boxId == TextBasedSmsColumns.MESSAGE_TYPE_INBOX ||
                    boxId == TextBasedSmsColumns.MESSAGE_TYPE_ALL) ?
                    true : false;
        } else {
            boxId = cursor.getInt(cursor.getColumnIndexOrThrow(Mms.MESSAGE_BOX));

            return (boxId == Mms.MESSAGE_BOX_INBOX || boxId == Mms.MESSAGE_BOX_ALL) ?
                    true : false;
        }
    }
}
