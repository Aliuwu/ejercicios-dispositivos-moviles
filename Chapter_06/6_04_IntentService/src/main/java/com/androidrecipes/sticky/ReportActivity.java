package com.androidrecipes.sticky;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Problema: La aplicación necesita ejecutar una o más operaciones en segundo plano que funcionarán incluso si el usuario cierra la aplicación
 * Solución: Crearemos un IntentService para manejar las operaciones.
 */
public class ReportActivity extends Activity {

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logEvent("CREATE");
    }

    /**
     * Inicia el evento
     */
    @Override
    public void onStart() {
        super.onStart();
        logEvent("START");
    }

    @Override
    public void onResume() {
        super.onResume();
        logEvent("RESUME");
    }

    /**
     * Pausa el evento
     */
    @Override
    public void onPause() {
        super.onPause();
        logWarning("PAUSE");
    }

    /**
     * Detiene el evento
     */
    @Override
    public void onStop() {
        super.onStop();
        logWarning("STOP");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        logWarning("DESTROY");
    }

    private void logEvent(String event) {
        Intent intent = new Intent(this, OperationsManager.class);
        intent.setAction(OperationsManager.ACTION_EVENT);
        intent.putExtra(OperationsManager.EXTRA_NAME, event);

        startService(intent);
    }

    private void logWarning(String event) {
        Intent intent = new Intent(this, OperationsManager.class);
        intent.setAction(OperationsManager.ACTION_WARNING);
        intent.putExtra(OperationsManager.EXTRA_NAME, event);

        startService(intent);
    }
}