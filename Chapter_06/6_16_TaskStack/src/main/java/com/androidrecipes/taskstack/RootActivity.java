package com.androidrecipes.taskstack;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;

/**
 * Problema: La aplicación permite a aplicaciones externas lanzar cierto tipo de actividades de forma directa y se necesita implementar unos patrones de navegación BACK vs UP apropiados
 * Solución: Usaremos las clases NavUtils y TaskStackBuilder de la librería Support
 */
public class RootActivity extends Activity implements View.OnClickListener {

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button listButton = new Button(this);
        listButton.setText("Show Family Members");
        listButton.setOnClickListener(this);

        setContentView(listButton,
                new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
    }

    public void onClick(View v) {
        //Lanza la siguiente actividad
        Intent intent = new Intent(this, ItemsListActivity.class);
        startActivity(intent);
    }
}
