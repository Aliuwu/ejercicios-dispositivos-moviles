package com.androidrecipes.logger;

import android.app.Activity;
import android.os.Bundle;

/**
 * Problema: Se necesita colocar sentencias de log en el código para debug o testeo
 * Solución: Aprovecharemos BuildConfig.DEBUG para las sentencias de log en la clase Log
 */
public class LoggerActivity extends Activity {

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //Esta sentencia solo se imprime en el modo debug
        Logger.d("Activity Created");
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Esta sentencia solo se imprime en el modo debug
        Logger.d("Activity Resume at %d", System.currentTimeMillis());
        //Esta sentencia siempre se imprime
        Logger.i("It is now %d", System.currentTimeMillis());
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Esta sentencia solo se imprime en el modo debug
        Logger.d("Activity Pause at %d", System.currentTimeMillis());
        //Esta sentencia siempre se imprime
        Logger.w("No, don't leave!");
    }
}
