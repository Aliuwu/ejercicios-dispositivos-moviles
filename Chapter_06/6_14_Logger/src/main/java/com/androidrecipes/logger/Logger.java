package com.androidrecipes.logger;

import android.util.Log;

public class Logger {
    private static final String LOGTAG = "AndroidRecipes";

    /**
     * Optimización mínima
     * @param format
     * @param args
     */
    private static String getLogString(String format, Object... args) {
        if (args.length == 0) {
            return format;
        }

        return String.format(format, args);
    }

    /**
     * Métodos para imprimir la información de advertencias y errores
     *
     */
    public static void e(String format, Object... args) {
        Log.e(LOGTAG, getLogString(format, args));
    }

    public static void w(String format, Object... args) {
        Log.w(LOGTAG, getLogString(format, args));
    }

    public static void w(Throwable throwable) {
        Log.w(LOGTAG, throwable);
    }

    public static void i(String format, Object... args) {
        Log.i(LOGTAG, getLogString(format, args));
    }


    /**
     * Logs en modo Debug
     *
     */
    public static void d(String format, Object... args) {
        if (!BuildConfig.DEBUG) return;

        Log.d(LOGTAG, getLogString(format, args));
    }

    public static void v(String format, Object... args) {
        if (!BuildConfig.DEBUG) return;

        Log.v(LOGTAG, getLogString(format, args));
    }
}
