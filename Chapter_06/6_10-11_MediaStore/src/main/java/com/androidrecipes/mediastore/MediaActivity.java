package com.androidrecipes.mediastore;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Problema: La aplicación necesita importar un elementos seleccionado por el usuario para mostrar o reproducir
 * Solución: Usaremos Intent.ACTION_GET_CONTENT para APIs de nivel 1 y Intent.ACTION_OPEN_DOCUMENT para APIs de nivel 19
 */
public class MediaActivity extends Activity implements View.OnClickListener {

    private static final int REQUEST_AUDIO = 1;
    private static final int REQUEST_VIDEO = 2;
    private static final int REQUEST_IMAGE = 3;

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Button images = (Button) findViewById(R.id.imageButton);
        images.setOnClickListener(this);
        Button videos = (Button) findViewById(R.id.videoButton);
        videos.setOnClickListener(this);
        Button audio = (Button) findViewById(R.id.audioButton);
        audio.setOnClickListener(this);

    }

    /**
     * Para verificar el tipo de solicitud recibida (imagen, video o audio)
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            Uri selectedContent = data.getData();

            if (requestCode == REQUEST_IMAGE) {
            }
            if (requestCode == REQUEST_VIDEO) {
            }
            if (requestCode == REQUEST_AUDIO) {
            }
        }
    }

    /**
     * Devuelve los resultados dependiendo del tipo de solicitud recibida
     * @param v
     */
    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        } else {
            intent.setAction(Intent.ACTION_GET_CONTENT);
        }

        intent.addCategory(Intent.CATEGORY_OPENABLE);
        switch (v.getId()) {
            case R.id.imageButton:
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_IMAGE);
                return;
            case R.id.videoButton:
                intent.setType("video/*");
                startActivityForResult(intent, REQUEST_VIDEO);
                return;
            case R.id.audioButton:
                intent.setType("audio/*");
                startActivityForResult(intent, REQUEST_AUDIO);
                return;
            default:
                return;
        }
    }
}