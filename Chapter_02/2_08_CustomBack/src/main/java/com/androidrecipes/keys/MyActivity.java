package com.androidrecipes.keys;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
/*carga cuatro instancias de fragmentos personalizados en una pila,
 de modo que la última que se agregue se muestre cuando se ejecute la aplicación.*/

public class MyActivity extends AppCompatActivity { //controla el comportamiento del botón Atrás


// La aplicación maneja al usuario presionando el botón BACK del hardware de manera personalizada.
//Carga cuatro instancias

    //
    //Construir una pila de fragmentos de UI
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // primer fragmento actúq como la vista de la raíz

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction(); //instancia de FragmentTransaction paara realizar transacciones de fragmentos
        ft.add(R.id.container_fragment, MyFragment.newInstance("First Fragment")); // agrega el fragmento first , lo especifica y espesifica la vista en la que se insertará ,  fragmento que quieres agregar.)
        ft.commit(); //actuliza y aplica los cambios

        // Crear un nuevo fragmento y transacción
        ft = getSupportFragmentManager().beginTransaction();
        // Reemplaza lo que esta en la vista fragment_container con second fragment,
         ft.replace(R.id.container_fragment, MyFragment.newInstance("Second Fragment"));
        // añade el fragmento a la entrada de la pila de atras de transaciones con nombre second
        ft.addToBackStack("second");
        // Confirmar la transacción
        ft.commit();

        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container_fragment, MyFragment.newInstance("Third Fragment"));
        ft.addToBackStack("third");
        ft.commit();

        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container_fragment, MyFragment.newInstance("Fourth Fragment"));
        ft.addToBackStack("fourth");
        ft.commit();
    }

    // lleva a los usuarios de vuelta al fragmento raíz
    public void onHomeClick(View v) {
          getSupportFragmentManager().popBackStack("second", FragmentManager.POP_BACK_STACK_INCLUSIVE); // lleva a la raiz saltando al segundo fragmento y eliminandolo  por la bandera POP_BACK_STACK_INCLUSIVE
    }

    //agrega un secion de interfaz de usuario independiente

    public static class MyFragment extends Fragment {
        private CharSequence mTitle;


        // Crear una nueva instancia de Fragment, proporcionando "title "
        public static MyFragment newInstance(String title) {
            MyFragment fragment = new MyFragment();
            fragment.setTitle(title);

            return fragment;
        }
        // carga al activity un diseño propio del fragmento
        @Override

        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) { //crea y devuelve la jerarquía de vistas asociada al fragmento.
            TextView text = new TextView(getActivity());
            text.setText(mTitle);
            return text;
        }

        //Establece el título de la barra de acción

        public void setTitle(CharSequence title) {
            mTitle = title;
        }
    }
}
