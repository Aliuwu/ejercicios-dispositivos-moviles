package com.androidrecipes.snackbar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SnackbarActivity extends AppCompatActivity {

    // Inicializamos la CONSTANTE NOMBRES, que será el array de nombres que se desplegaran
    // como items de la lista
    private static String[] NOMBRES = {"I look", "Inside myself", "And see", "My hearth",
    "Is black", "No colors", "Anymore", "I want them", "To turn black"};

    // Declaramos el adapter para el RecyclerView
    private NameAdapter mAdapter;
    @Nullable
    private ArrayList<String> mClearedNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Se instancia el objeto listOfNames de la clase RecyclerView
        // y se le asocia el elemento recyclerView con id list_of__names
        // en el xml.
        RecyclerView listOfNames = (RecyclerView) findViewById(R.id.list_of_names);
        listOfNames.setHasFixedSize(true);
        mAdapter = new NameAdapter();
        // Se establece el adapter adapter al objeto listOfNames
        listOfNames.setAdapter(mAdapter);
        // Si el estado guardado de la instancia del activity es null, se agrega el array NOMBRES
        // como ArrayList al objeto mAdapter
        if (savedInstanceState == null) {
            mAdapter.addNames(Arrays.asList(NOMBRES));
        } else {
            ArrayList<String> names = savedInstanceState.getStringArrayList("names");
            mAdapter.addNames(names);
            if (savedInstanceState.containsKey("clearedNames")) {
                ArrayList<String> clearedNames = savedInstanceState
                        .getStringArrayList("clearedNames");
                showClearSnackbar(clearedNames);
            }
        }

        // Al hacer click en el boton con id fab, se limpia la lista de elementos
        // del objeto mAdapter y se despliega el Snackbar
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter.clearList();
            }
        });
    }

//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        outState.putStringArrayList("names", mAdapter.mNames);
//        if (mClearedNames != null) {
//            outState.putStringArrayList("clearedNames", mClearedNames);
//        }
//    }

    // Verifica si existe main.xml, le asigna las caracteristicas de menú y devuelve true;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);
        return true;
    }

    // Acciones del menú para añadir items a la lista
    // Método para añadir una nueva lista de items a continuacion de una existentente, si la hay dentro
    // del objeto del tipo Recycler View en el xml.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_items:
                mAdapter.addNames(Arrays.asList(NOMBRES));
                return true;
            default:
                return false;
        }
    }

    public void doClearList(View view) {
    }

    // Metodo para crear el Snackbar dentro del layout del activity_main
    private void showClearSnackbar(final ArrayList<String> names) {
        Snackbar.make(findViewById(R.id.activity_main),
                getString(R.string.names_deleted, names.size()),
                Snackbar.LENGTH_INDEFINITE)
                // Se llama al callback() cuando el Snackbar es mostrado, y tambien
                // cuando es descartado.
                .setCallback(new Snackbar.Callback() {

                    // Cuando se descarta el Snackbar el ArrayList names queda con el valor de
                    // null (no tiene nombres)
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        super.onDismissed(snackbar, event);
                        mClearedNames = null;
                    }
                    // Cuando se muestra el Snackbar el Arraylist names es igual al Arraylist
                    // de nombres dentro del RecyclerView
                    @Override
                    public void onShown(Snackbar snackbar) {
                        super.onShown(snackbar);
                        mClearedNames = names;
                    }
                })

                // Se establece el escuchador de eventos de click a la cadena undo_label,
                // cuando se llame al evento click, se vuelve a añadir al objeto mAdapter,
                // el array de nombres definidos que se le pasa al metodo para mostrar el Snackbar,
                // que es el mismo array de nombres que componen el objeto mAdapter en el RecyclerView
                .setAction(R.string.undo_label, new View.OnClickListener() { // Accion Deshacer
                    @Override
                    public void onClick(View v) {
                        // Undo click - revert delete action
                        mAdapter.addNames(names);
                    }
                })
                .show();
    }

    private static class NameViewHolder extends RecyclerView.ViewHolder {
        TextView nameView;

        NameViewHolder(View itemView) {
            super(itemView);
            nameView = (TextView) itemView.findViewById(android.R.id.text1);
        }
    }

    // Clase privada NameAdapter, para definir el adapter personalizado que se va a añadir al recycler view.

    private class NameAdapter extends RecyclerView.Adapter<NameViewHolder> {
        private final ArrayList<String> mNames;

        NameAdapter() {
            mNames = new ArrayList<>();
        }

        @Override
        public NameViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(SnackbarActivity.this)
                    .inflate(android.R.layout.simple_list_item_1, parent, false);
            return new NameViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(NameViewHolder holder, int position) {
            holder.nameView.setText(mNames.get(position));
        }

        @Override
        public int getItemCount() {
            return mNames.size();
        }

        void clearList() {
            int oldSize = mNames.size();
            ArrayList<String> oldNames = new ArrayList<>(mNames);
            mNames.clear();
            if (oldSize > 0) {
                notifyItemRangeRemoved(0, oldSize);
                showClearSnackbar(oldNames);
            }
        }

        void addNames(List<String> names) {
            int oldSize = mNames.size();
            mNames.addAll(names);
            notifyItemRangeInserted(oldSize, names.size());
        }
    }
}
