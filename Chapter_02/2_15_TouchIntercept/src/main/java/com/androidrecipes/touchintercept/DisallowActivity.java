package com.androidrecipes.touchintercept;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

//Problema: Cuando existen varios componentes táctiles suelen presentarse problemas en cuanto a la jerarquía de estos,

//Solución: Usaremos el método requestDisallowTouchIntercept() esto ayudará a que no existan interceptaciones del container en los eventos de los child views

public class DisallowActivity extends Activity implements
        ViewPager.OnPageChangeListener {
    //Creamos un array de tipo String para los items
    private static final String[] ITEMS = {
            "Row One", "Row Two", "Row Three", "Row Four",
            "Row Five", "Row Six", "Row Seven", "Row Eight",
            "Row Nine", "Row Ten", "Row Eleven", "Row Twelve", "Row Thirteen"
    };
    //Creamos el viewPager que servirá para cambiar entre los items horizontales
    private ViewPager mViewPager;

    //Creamos un listview para desplegar los items
    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Configuramos el ViewPager y el ListView

        // Creamos un encabezado para cambiar entre los items horizontales
        mViewPager = new ViewPager(this);
        // Ajustamos la altura del ViewPager
        mViewPager.setLayoutParams(new ListView.LayoutParams(
                ListView.LayoutParams.MATCH_PARENT,
                getResources().getDimensionPixelSize(R.dimen.header_height)));
        // Cambios de estado del pager
        //mViewPager.setOnPageChangeListener(this);
        mViewPager.addOnPageChangeListener(this);
        mViewPager.setAdapter(new HeaderAdapter(this));

        // Creamos un listView para deplegar las filas
        mListView = new ListView(this);
        // Agregamos el ViewPager como un encabezado del listView
        mListView.addHeaderView(mViewPager);
        // Añadimos los items de la lista
        mListView.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, ITEMS));

        setContentView(mListView);
    }

    /* Métodos OnPageChangeListener  */

    @Override
    public void onPageScrolled(int position,
                               float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
    }


    @Override
    public void onPageScrollStateChanged(int state) {
        //Mientras se desliza en el ViewPager se desahibilita el scroll vertical
        // This flag must be set for each gesture you want to override.
        boolean isScrolling = state != ViewPager.SCROLL_STATE_IDLE;
        mListView.requestDisallowInterceptTouchEvent(isScrolling);
    }

    private static class HeaderAdapter extends PagerAdapter {
        private Context mContext;

        public HeaderAdapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            return 5;
        } //Número de los pager del ViewPage

        //Crea una página en la posición dada
        @Override
        public Object instantiateItem(ViewGroup container,
                                      int position) {
            // Create a new page view
            TextView tv = new TextView(mContext);
            tv.setText(String.format("Page %d", position + 1));
            tv.setBackgroundColor((position % 2 == 0) ? Color.RED
                    : Color.GREEN);
            tv.setGravity(Gravity.CENTER);
            tv.setTextColor(Color.BLACK);

            // Retorna el objeto en la posición (Añade la nueva view)
            container.addView(tv);
            return tv;
        }

        @Override
        public void destroyItem(ViewGroup container,
                                int position, Object object) {
            View page = (View) object;
            container.removeView(page);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }
    }
}
