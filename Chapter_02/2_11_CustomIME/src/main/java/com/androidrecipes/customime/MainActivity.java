package com.androidrecipes.customime;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity implements TextView.OnEditorActionListener { //Definición de la interfaz para que se invoque una llamada de retorno cuando se realice una acción en el editor.
    //variables de timpo EditTe
    EditText text1;
    EditText text2;
    EditText text3;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        text1 = (EditText)findViewById(R.id.text1); //asocia al editext del xml con  un casting para que resulte un objeto, buscando una vista determinada por su id dentro de un layout especifico
        text1.setOnEditorActionListener(this);  //cuando se llame realiza una ación en la vista de texto
        text2 = (EditText)findViewById(R.id.text2);
        text2.setOnEditorActionListener(this);
         text3 = (EditText)findViewById(R.id.text3);
        text3.setOnEditorActionListener(this);

    }

    //Definir las acciones que realizarán las teclas intro al ser presionadas
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) { //llamado cuando realiza una acion (vista en la que se hizo clic,Identificador de la acción,Si es activado por una tecla )
        if(actionId == EditorInfo.IME_ACTION_SEARCH) {// Muestra una ventana de búsqueda como la tecla Inter
            return true;
        }
        if(actionId == EditorInfo.IME_ACTION_GO) { //Muestra Ir como la tecla Inter
            return true;
            }
        if(actionId == EditorInfo.IME_ACTION_NEXT) { //Muestra Next como la tecla Enter
            return true;
        }

        return false; //cuando no maneja ningun evento
    }
}
