package com.androidrecipes.rotation;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.EditText;

public class ManualRotationActivity extends Activity {

    private EditText mEditText;
    private CheckBox mCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadView();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        //Only reload the view under the new configuration
        // if the box is checked.
        if (mCheckBox.isChecked()) {
            final Bundle uiState = new Bundle();
            //Almacena el estado de la interfaz de usuario
            saveState(uiState);
            //Recarga la vista
            loadView();
            //Restaura el estado de la interfaz de usuario
            restoreState(uiState);
        }
    }

    private void saveState(Bundle state) {
        state.putBoolean("checkbox", mCheckBox.isChecked());
        state.putString("text", mEditText.getText().toString());
    }

    //Restaura los elementos guardados antes de recargar
    private void restoreState(Bundle state) {
        mCheckBox.setChecked(state.getBoolean("checkbox"));
        mEditText.setText(state.getString("text"));
    }

    //Asigna el contenedor de la vista
    private void loadView() {
        setContentView(R.layout.activity_manual);

        //Reseteamos los elelemtos de la vista
        mCheckBox = (CheckBox) findViewById(R.id.override);
        mEditText = (EditText) findViewById(R.id.text);
    }
}
