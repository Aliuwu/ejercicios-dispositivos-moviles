package com.androidrecipes.viewpager;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class ImagePagerAdapter extends PagerAdapter {
    //listado de imagenes
    private static final int[] IMAGES = {
            android.R.drawable.ic_menu_camera,
            android.R.drawable.ic_menu_add,
            android.R.drawable.ic_menu_delete,
            android.R.drawable.ic_menu_share,
            android.R.drawable.ic_menu_edit
    };
    //Listado de colores
    private static final int[] COLORS = {
            Color.RED,
            Color.BLUE,
            Color.GREEN,
            Color.GRAY,
            Color.MAGENTA
    };
    // Contenedor
    private Context mContext;

    public ImagePagerAdapter(Context context) {
        super();
        mContext = context;
    }

    /*
     * Provide the total number of pages
     */
    @Override
    public int getCount() {
        return 5;
    }
    // este metodo es para devolver la cantidad de elementos disponibles

    // se reescribe si se quiere mostrar mas de una pagina en el viewpager

    //devuelve el tamanio que le corresponde a la pagina del viewpager
    @Override
    public float getPageWidth(int position) {
        return 0.333f;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        // Crear un nuevo imagen view y agregarlo al contenedor
        ImageView iv = new ImageView(mContext);
        // enviar el contenido de acuerdo a la posicion
        iv.setImageResource(IMAGES[position]);
        iv.setBackgroundColor(COLORS[position]);
        // Se agrela a la vista
        container.addView(iv);
        //devuelve la vista como objeto clave
        return iv;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // lo eliminia del contenedor
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        // valida que el objeto devuelto del instant tenga la localizacion en el contenedor
        // la app enlaza el objeto clave con la vista
        return (view == object);
    }

}
