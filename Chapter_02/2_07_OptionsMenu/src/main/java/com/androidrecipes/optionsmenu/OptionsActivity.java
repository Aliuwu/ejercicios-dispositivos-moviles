package com.androidrecipes.optionsmenu;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.CompoundButton;

public class OptionsActivity extends AppCompatActivity implements
        PopupMenu.OnMenuItemClickListener,
        CompoundButton.OnCheckedChangeListener {

    private MenuItem mOptionsItem; //item menu
    private CheckBox mFirstOption, mSecondOption; //checkbox de widget

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    // se da al tocar la tecla menu, o una activida con barra de accion
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options, menu); // crea menus a partir de xml

        //Inicializa el item menu de opciones
        mOptionsItem = menu.findItem(R.id.menu_add);
        //activa detectar la opcion seleccionad
        MenuItemCompat.setOnActionExpandListener(mOptionsItem, new MenuItemCompat.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                //devuelve verdadero para que se expanda
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {//Que hacer cuando colapsa
                mFirstOption.setChecked(false);
                mSecondOption.setChecked(false);
                //desactiva los checkbox
                return true;
            }
        });

        //Crear los checkbox de xml
        mFirstOption = (CheckBox) MenuItemCompat.getActionView(mOptionsItem).findViewById(R.id.option_first);
        mFirstOption.setOnCheckedChangeListener(this);
        mSecondOption = (CheckBox) MenuItemCompat.getActionView(mOptionsItem).findViewById(R.id.option_second);
        mSecondOption.setOnCheckedChangeListener(this);

        return true;
    }
    
    /* CheckBox */

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        // cuando ambos checkbox son verdaderos colapsa el widget
        if (mFirstOption.isChecked() && mSecondOption.isChecked()) {
            MenuItemCompat.collapseActionView(mOptionsItem);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // Se configura las acciones que deben aparecer cuando se abre el menu
        return super.onPrepareOptionsMenu(menu);
    }

    //Callback from the PopupMenu click
    public boolean onMenuItemClick(MenuItem item) {
        menuItemSelected(item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // opcion que se seleciona del menu
        menuItemSelected(item);
        return true;
    }

    private void menuItemSelected(MenuItem item) {
        //con la opcion seleccionada, por id, escoge la accion
        switch (item.getItemId()) {
            case R.id.menu_add:
                //Acción
                break;
            case R.id.menu_remove:
                //Acción
                break;
            case R.id.menu_edit:
                //Acción
                break;
            case R.id.menu_settings:
                //Acción
                break;
            default:
                break;
        }
    }
}
