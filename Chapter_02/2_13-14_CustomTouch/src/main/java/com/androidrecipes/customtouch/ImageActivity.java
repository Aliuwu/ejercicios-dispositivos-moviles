package com.androidrecipes.customtouch;

import android.app.Activity;
import android.os.Bundle;

public class ImageActivity extends Activity {

    /**
     * Inicializa la actividad
     * Obtiene el recurso de la imagen
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RotateZoomImageView imageView = new RotateZoomImageView(this);
        imageView.setImageResource(R.drawable.ic_launcher);

        setContentView(imageView);
    }
}
