package com.androidrecipes.customtouch;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

public class RemoteScrollActivity extends Activity implements View.OnTouchListener {

    private TextView mTouchText;
    private HorizontalScrollView mScrollView;

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mTouchText = (TextView) findViewById(R.id.text_touch);
        mScrollView = (HorizontalScrollView) findViewById(R.id.scroll_view);
        mTouchText.setOnTouchListener(this);
    }

    /**
     * Asigna la ubicación vertical cada evento
     * @param v
     * @param event
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        event.setLocation(event.getX(), mScrollView.getHeight() / 2);
        mScrollView.dispatchTouchEvent(event);
        return true;
    }
}
