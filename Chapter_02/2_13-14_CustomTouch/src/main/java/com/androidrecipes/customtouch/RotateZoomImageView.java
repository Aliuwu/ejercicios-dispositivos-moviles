package com.androidrecipes.customtouch;

import android.content.Context;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.widget.ImageView;

public class RotateZoomImageView extends ImageView {

    private ScaleGestureDetector mScaleDetector;
    private Matrix mImageMatrix;
    /* Último ángulo de rotación*/
    private int mLastAngle = 0;
    /* Punto de pivote para las transformaciones */
    private int mPivotX, mPivotY;

    /**
     * Constructor 1
     * @param context
     */
    public RotateZoomImageView(Context context) {
        super(context);
        init(context);
    }

    /**
     * Constructor 2
     * @param context
     * @param attrs
     */
    public RotateZoomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    /**
     * Constructor 3
     * @param context
     * @param attrs
     * @param defStyle
     */
    public RotateZoomImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    /**
     * Inicializa el ScaleGestureDectector
     * @param context
     */
    private void init(Context context) {
        mScaleDetector = new ScaleGestureDetector(context, mScaleListener);

        setScaleType(ScaleType.MATRIX);
        mImageMatrix = new Matrix();
    }

    /**
     * Calcula los valores basados en el tamaño de la vista
     * @param w
     * @param h
     * @param oldw
     * @param oldh
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (w != oldw || h != oldh) {
            //Ajustar la imagen al centro de la vista
            int translateX = Math.abs(w - getDrawable().getIntrinsicWidth()) / 2;
            int translateY = Math.abs(h - getDrawable().getIntrinsicHeight()) / 2;
            mImageMatrix.setTranslate(translateX, translateY);
            setImageMatrix(mImageMatrix);

            mPivotX = w / 2;
            mPivotY = h / 2;
        }
    }

    /**
     * Activa el ScaleGestureDetector
     */
    private SimpleOnScaleGestureListener mScaleListener = new SimpleOnScaleGestureListener() {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            //Factor de escala para la imagen
            float scaleFactor = detector.getScaleFactor();

            mImageMatrix.postScale(scaleFactor, scaleFactor, mPivotX, mPivotY);
            setImageMatrix(mImageMatrix);

            return true;
        }
    };

    /**
     * Calcula el ángulo de cambio entre los punteros y rota la imagen de acuerdo a esto
     * @param event
     * @return
     */
    private boolean doRotationEvent(MotionEvent event) {
        //Calcula el ángulo entre dos dedos
        float deltaX = event.getX(0) - event.getX(1);
        float deltaY = event.getY(0) - event.getY(1);
        double radians = Math.atan(deltaY / deltaX);
        //Convertir a grados
        int degrees = (int) (radians * 180 / Math.PI);

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //Ángulo inicial
                mLastAngle = degrees;
                break;
            case MotionEvent.ACTION_MOVE:

                if ((degrees - mLastAngle) > 45) {

                    mImageMatrix.postRotate(-5, mPivotX, mPivotY);
                } else if ((degrees - mLastAngle) < -45) {

                    mImageMatrix.postRotate(5, mPivotX, mPivotY);
                } else {

                    mImageMatrix.postRotate(degrees - mLastAngle, mPivotX, mPivotY);
                }

                setImageMatrix(mImageMatrix);
                //Guarda el ángulo actual
                mLastAngle = degrees;
                break;
        }

        return true;
    }

    /**
     * Multitouch event
     * @param event
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            return true;
        }

        switch (event.getPointerCount()) {
            case 3:
                // Con 3 dedos realizae zoom en la imagen
                return mScaleDetector.onTouchEvent(event);
            case 2:
                // Con dos dedos rota la imagen
                return doRotationEvent(event);
            default:
                return super.onTouchEvent(event);
        }
    }
}
