package com.androidrecipes.alertdialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class DialogActivity extends Activity
        implements DialogInterface.OnClickListener, View.OnClickListener {

    // Se declara una constante de tipo String[] que solo podra ser accesada dentro de DialogActivity.java
    private static final String[] OPCIONES =
            {"Opcion 1", "Opcion 2", "Opcion 3", "Opcion 4", "Opcion 5"};

    // Se instancia los elementos de UI.
    Button mButton;
    AlertDialog mActions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Se iniciliza el boton, se le asigna un value y un escuchador de evento click.
        mButton = new Button(this);
        mButton.setText("Mostrar Alert Dialog");
        mButton.setOnClickListener(this);

        // Se instancia el objeto para crear la caja de dialogo pop-up
        // Se coloca el titulo de la pop-up, los items de la lista (opciones que se pueden
        // seleccionar)
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Lista multiopción");
        builder.setItems(OPCIONES, this);

        // Muestra un mensaje simple al usuario
        // builder.setMessage("Puedes escoger alguna de estas opciones");


        // Se añade el boton cancelar, que tiene como funcion unica cerrar la pop-up
        // pero no tiene asignado un escuchador de eventos para ejecutar otra funcionalidad.
        builder.setNegativeButton("Cancel", null);
        //Se crea el alertDialog a partir del objeto builder.
        mActions = builder.create();

        setContentView(mButton);
    }

    //List selection action handled here
    // Método para elegir una opcion de la lista disponible del alertDialog y mostrar su valor en
    // en el value de mButton.
    @Override
    public void onClick(DialogInterface dialog, int pos) {
        // Se elije uno de los elementos del array de las opciones de pop-up
        String selected = OPCIONES[pos];
        // Se establece como value del boton, el valor del elemento seleccionado dentro del array
        mButton.setText(selected);
    }

    //Button action handled here (pop up the dialog)
    // Al llamarse al evento onClick se muestra del alertDialog(pop-up)
    @Override
    public void onClick(View v) {
        mActions.show();
    }
}
