package com.androidrecipes.alertdialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

public class CustomItemActivity extends Activity
        implements DialogInterface.OnClickListener, View.OnClickListener {

    private static final String[] OPCIONES =
            {"Opcion 1", "Opcion 2", "Opcion 3", "Opcion 4", "Opcion 5"};
    private static final String[] DETALLES =
            {"Detalle 1", "Detalle 2", "Detalle 3", "Detalle 4", "Detalle 5"};

    Button mButton;
    AlertDialog mActions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mButton = new Button(this);
        mButton.setText("Mostrar Alert Dialog");
        mButton.setOnClickListener(this);

        // Array adapter para pasar al builder (opciones del pop-up)
        // Este adapter tiene una implementación personalizada de getView() que retorna un layout personalizado, definido en xml,
        // que tiene 2 text labels, uno alineado a la izquierda y otro a la derecha
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_item) {
            @NonNull
            @Override
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                View row = convertView;
                if (row == null) {
                    row = getLayoutInflater().inflate(R.layout.list_item, parent, false);
                }

                TextView name = (TextView) row.findViewById(R.id.text_name);
                TextView detail = (TextView) row.findViewById(R.id.text_detail);

                name.setText(OPCIONES[position]);
                detail.setText(DETALLES[position]);

                return row;
            }

            @Override
            public int getCount() {
                return OPCIONES.length;
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Lista multiopción");

        // Le proporcionamos el adapater al builder en lugar de pasarle un simple array de elementos.
        builder.setAdapter(adapter, this);
        //Cancel action does nothing but dismiss, we could add another listener here
        // to do something extra when the user hits the Cancel button
        builder.setNegativeButton("Cancel", null);
        mActions = builder.create();

        setContentView(mButton);
    }

    @Override
    public void onClick(DialogInterface dialog, int pos) {
        String selected = OPCIONES[pos];
        mButton.setText(selected);
    }


    @Override
    public void onClick(View v) {
        mActions.show();
    }
}
