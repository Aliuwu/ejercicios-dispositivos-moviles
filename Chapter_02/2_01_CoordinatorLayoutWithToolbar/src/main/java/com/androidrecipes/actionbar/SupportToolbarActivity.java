package com.androidrecipes.actionbar;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

public class SupportToolbarActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar);


        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }
    /// Dentro de onCreate (), somos responsables de pasar a la actividad una referencia a la barra de herramientas dentro del diseño
    //a través de setSupportActionBar (). Esto permite que la actividad aplique el menú de opciones y otros elementos específicos a esa vista.
    //La actividad establece los valores de título de una instancia de la barra de herramientas después de que se complete onCreate (). Esto significa que cualquier
    // debemos hacer nuestros cambios en onPostCreate () para que nuestros cambios se mantengan.
    //Si ejecuta esta actividad, debería verse exactamente igual
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mToolbar.setTitle("Android");
        mToolbar.setSubtitle("Toolbar Recipes");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.support, menu);
        return true;
    }
}
