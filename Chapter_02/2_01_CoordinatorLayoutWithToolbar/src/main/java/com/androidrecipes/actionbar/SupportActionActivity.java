package com.androidrecipes.actionbar;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


// Problemaa: Desea usar los últimos patrones de barra de acción en su aplicación, mientras se mantiene compatible con
//  dispositivos más antiguos, y desea personalizar el aspecto para que coincidan con el tema de su aplicación.

public class SupportActionActivity extends AppCompatActivity {


    //Obtenemos una referencia a la barra de acción en onCreate () a través de getSupportActionBar (), y comenzamos
    //estableciendo sus atributos de título. El subtítulo es opcional, pero si no se proporciona un título, se mostrara un error.
    //Usando el método setDisplayHomeAsUpEnabled (), podemos activar la flecha hacia arriba opcional.
    //Esto se usa para proporcionar navegación de regreso a una actividad principal, y generalmente no está habilitado en el nivel superior
    //ocupaciones. El comportamiento del botón Arriba está definido por la aplicación. Los clics del usuario en el botón activarán
    //en el método onOptionsItemSelected () de la actividad con el valor android.R.id.home.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();


        actionBar.setDisplayHomeAsUpEnabled(true);

        actionBar.setTitle("Android Recipes");

        actionBar.setSubtitle("ActionBar Recipes");

        // Esto publicara una iamgen en el sector del titulo y subtitulo.
//
//        actionBar.setDisplayShowCustomEnabled(true);
//        ImageView imageView = new ImageView(this);
//        imageView.setImageResource(R.drawable.ic_launcher);
//        imageView.setScaleType(ImageView.ScaleType.CENTER);
//        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(
//                ActionBar.LayoutParams.MATCH_PARENT,
//                ActionBar.LayoutParams.MATCH_PARENT);
//        actionBar.setCustomView(imageView, lp);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.support, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
