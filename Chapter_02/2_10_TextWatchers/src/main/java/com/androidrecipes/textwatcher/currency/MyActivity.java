package com.androidrecipes.textwatcher.currency;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;

public class MyActivity extends Activity {
    //Se aplica este formateador de texto personalizado a EditText en esta activity
    EditText text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        text = new EditText(this);
        text.addTextChangedListener(new CurrencyTextWatcher());

        setContentView(text);
    }

}
