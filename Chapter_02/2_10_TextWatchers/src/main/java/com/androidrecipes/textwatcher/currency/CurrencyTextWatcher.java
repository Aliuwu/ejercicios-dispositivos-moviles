package com.androidrecipes.textwatcher.currency;

import android.text.Editable;
import android.text.TextWatcher;

import java.text.NumberFormat;

public class CurrencyTextWatcher implements TextWatcher {
    /*Su trabajo es aplicar el formato estándar para los usuarios.
     * mientras escriben, reduciendo la cantidad de teclas necesarias para ingresar datos legibles.*/
    boolean mEditing;

    public CurrencyTextWatcher() {
        mEditing = false;
    }
    //Crea un CurrencyTextWatcher para insertar el símbolo de moneda y el punto de separación
    //en un TextView.
    //Este método se llama para notificarle que, en algún lugar dentro s, el texto ha sido cambiado.
    public synchronized void afterTextChanged(Editable s) {//el valor tecleado
        if (!mEditing) {
            mEditing = true;//pasa a verdadero si se ha cambiado el estado del botón

            //Strip symbols
            String digits = s.toString().replaceAll("\\D", "");
            NumberFormat nf = NumberFormat.getCurrencyInstance();//formato de moneda
            try {
                String formatted = nf.format(Double.parseDouble(digits) / 100);//toma el dígito
                s.replace(0, s.length(), formatted);
            } catch (NumberFormatException nfe) {
                s.clear();
            }

            mEditing = false;
        }
    }
    //Se llama a este método para notificarle que, dentro de s, los countcaracteres que comienzan a start punto de ser reemplazados por un nuevo texto con longitud after.
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

}
