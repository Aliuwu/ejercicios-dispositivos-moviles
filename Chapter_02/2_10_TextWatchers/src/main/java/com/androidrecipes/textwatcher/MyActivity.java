package com.androidrecipes.textwatcher;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class MyActivity extends Activity implements TextWatcher {

    EditText text;
    int textCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Crea un EditText y agregue el observador
        text = new EditText(this);
        text.addTextChangedListener(this);

        setContentView(text);
    }

    /* TextWatcher Implementation Methods
     * Un uso simple de TextWatcher es crear un contador de caracteres que siga un EditText a medida que el usuario escribe
     * o elimina información.
     * */

    /*
     * Debido a que nuestras necesidades no incluyen la modificación del texto que se está insertando, podemos leer el recuento desde
     * onTextChanged (), que ocurre tan pronto como se produce el cambio de texto. Los otros métodos no se usan y se dejan
     * vacío*/
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int end) {
        textCount = text.getText().length();
        setTitle(String.valueOf(textCount));
    }

    public void afterTextChanged(Editable s) {
    }

}