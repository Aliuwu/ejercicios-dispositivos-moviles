package com.androidrecipes.popupmenus;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

//Crear acciones contextuales (Menús contextuales)
//Problema: Se requiere proveer al usuario con múltiples acciones para tomar, como resultado de seleccionar cierta parte de la interfaz de usuario

///SOLUCIÓN:
 //Necesitamos usar un PopupMenu para mostrar las acciones contextuales, si se seleccionan varios items se activa el modo ActionMode
public class ActionActivity extends AppCompatActivity implements AbsListView.MultiChoiceModeListener {

    //Creamos un array de tipo String con los nombres de los items
    private static final String[] ITEMS = {
            "Mom", "Dad", "Brother", "Sister", "Uncle", "Aunt",
            "Cousin", "Grandfather", "Grandmother"};

    //Creamos un listview para mostrar la lista de items
    private ListView mList;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mList = new ListView(this);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.list_item, R.id.text, ITEMS);
        mList.setAdapter(adapter);
        mList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        mList.setMultiChoiceModeListener(this);

        setContentView(mList, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
    }

    //Crear ActionMode
    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater(); //Creamos un menú a partir de los recursos del layout xml
        inflater.inflate(R.menu.contextmenu, menu);
        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        SparseBooleanArray items = mList.getCheckedItemPositions();

        //Cuando se seleccionan múltiples items entre en el siguiente switch
        switch (item.getItemId()) {
            case R.id.menu_delete:
                Toast.makeText(this, "Borrar ", Toast.LENGTH_SHORT).show();
                System.out.println("Borrar");
                break;
            case R.id.menu_edit:
                System.out.println("Editar");
                Toast.makeText(this, "Editar ", Toast.LENGTH_SHORT).show();
                break;
            default:
                return false;
        }
        return true;
    }


    //Nos indica cuantos items han sido seleccionados
    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position,
                                          long id, boolean checked) {
        int count = mList.getCheckedItemCount();
        mode.setTitle(String.format("%d Selected", count));
    }
}
