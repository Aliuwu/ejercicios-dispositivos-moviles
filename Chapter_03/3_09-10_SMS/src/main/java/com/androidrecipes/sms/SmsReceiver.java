package com.androidrecipes.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.widget.Toast;

public class SmsReceiver extends BroadcastReceiver {
    //Dispositivo desde el que se va a enviar el mensaje
    private static final String SENDER_ADDRESS = "+593980450473";

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();

        Object[] messages = (Object[]) bundle.get("pdus");
        SmsMessage[] sms = new SmsMessage[messages.length];
        //Crea los mensajes
        for (int n = 0; n < messages.length; n++) {
            sms[n] = SmsMessage.createFromPdu((byte[]) messages[n]);
        }
        for (SmsMessage msg : sms) {
            //Verifica de donde viene el mensaje
            if (TextUtils.equals(msg.getOriginatingAddress(), SENDER_ADDRESS)) {
                abortBroadcast();
                //Muestra la notificación
                Toast.makeText(context,
                        "Received message from the mothership: "
                                + msg.getMessageBody(),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}

