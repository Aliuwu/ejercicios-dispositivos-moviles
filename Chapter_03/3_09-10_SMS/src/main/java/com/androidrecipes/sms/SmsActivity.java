package com.androidrecipes.sms;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Problema: La aplicación debe reaccionar a los mensajes sms que reciba
 * Solución: Usaremos BroadcastReceiver para recibir los mensajes y procesarlos con el método onReceive()
 */
@TargetApi(19)
public class SmsActivity extends Activity {
    private static final String ACTION_SENT =
            "com.examples.sms.SENT";
    private static final String ACTION_DELIVERED =
            "com.examples.sms.DELIVERED";
    /**
     * Dirección del dispositivo al que se le quieren enviar los mensajes
     */
    String RECIPIENT_ADDRESS= "";

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.SEND_SMS},1);
        final Button sendButton=findViewById(R.id.btn_enviar);

        /**
         * Ubicamos los componentes de la interfaz
         */
        RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        rel_btn.leftMargin = 150;
        rel_btn.topMargin = 800;
        rel_btn.width = 350;
        rel_btn.height = 180;

       sendButton.setLayoutParams(rel_btn);

        final EditText numero=findViewById(R.id.txt_numero);
        RelativeLayout.LayoutParams rel_txt = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        rel_txt.leftMargin = 150;
        rel_txt.topMargin = 50;
        rel_txt.width = 400;
        rel_txt.height = 100;
        numero.setLayoutParams(rel_txt);

        final EditText msj=findViewById(R.id.txt_msj);
        RelativeLayout.LayoutParams rel_msj = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        rel_msj.leftMargin = 150;
        rel_msj.topMargin = 150;
        rel_msj.width = 400;
        rel_msj.height = 100;
        msj.setLayoutParams(rel_msj);

        final TextView lbl_num=findViewById(R.id.lbl_numero);
        RelativeLayout.LayoutParams rel_ln = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        rel_ln.rightMargin = 20;
        rel_ln.topMargin = 110;
        rel_ln.width = 200;
        rel_ln.height = 100;
        lbl_num.setLayoutParams(rel_ln);

        final TextView lbl_msj=findViewById(R.id.lbl_msj);
        RelativeLayout.LayoutParams rel_lm = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        rel_lm.rightMargin = 20;
        rel_lm.topMargin = 210;
        rel_lm.width = 200;
        rel_lm.height = 100;
        lbl_msj.setLayoutParams(rel_lm);

        /**
         * Método para enviar los mensajes
         */
        sendButton.setText("Enviar");
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 String codigo="+593";
                 String num=numero.getText().toString();
                 RECIPIENT_ADDRESS= codigo + (num.substring(1, num.length()));
                 sendSMS(msj.getText().toString());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Monitorea el estado de las operaciones
        registerReceiver(sent, new IntentFilter(ACTION_SENT));
        registerReceiver(delivered, new IntentFilter(ACTION_DELIVERED));
    }

    /**
     * Pausa la actividad
     */
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(sent);
        unregisterReceiver(delivered);
    }

    private void sendSMS(String message) {
        //Para enviar el sms
        PendingIntent sIntent = PendingIntent.getBroadcast(
                this, 0, new Intent(ACTION_SENT), 0);
        //Confirmación de entrega del sms
        PendingIntent dIntent = PendingIntent.getBroadcast(
                this, 0, new Intent(ACTION_DELIVERED), 0);

        //Envía el mensaje
        SmsManager manager = SmsManager.getDefault();
        manager.sendTextMessage(RECIPIENT_ADDRESS, null, message,
                sIntent, dIntent);
    }

    /*
     * BroadcastReceiver que es registrado para recibir eventos cuando el sms es enviado
     */
    private BroadcastReceiver sent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    //La actividad se realizó sin problemas
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                case SmsManager.RESULT_ERROR_NULL_PDU:
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    //Ocurrió un error al enviar el mensaje
                    break;
            }
        }
    };

    /*
     * BroadcastReceiver that is registered to receive events when
     * an SMS delivery confirmation is received; with the result code.
     */
    private BroadcastReceiver delivered = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    //Handle delivery success
                    break;
                case Activity.RESULT_CANCELED:
                    //Handle delivery failure
                    break;
            }
        }
    };
}

