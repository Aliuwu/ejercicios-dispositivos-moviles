package com.androidrecipes.webview;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

/**
 * Problema: Se necesita presentar información de tipo HTML o imágenes provenientes de la web sin ninguna modificación
 * Solución: Usaremos un widget WebView que está basado en el motor con el que trabaja el buscador de Android
 */
public class MyActivity extends Activity {
    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WebView webview = new WebView(this);
        //Habilita el soporte de JavaScript
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadUrl("http://www.android.com/");

        setContentView(webview);
    }
}

