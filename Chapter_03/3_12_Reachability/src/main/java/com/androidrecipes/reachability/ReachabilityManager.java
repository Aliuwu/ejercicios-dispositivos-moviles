package com.androidrecipes.reachability;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class ReachabilityManager {

    private ConnectivityManager mManager;

    private static ReachabilityManager instance;

    public static synchronized ReachabilityManager getInstance(Context context) {
        if (instance == null) {
            instance = new ReachabilityManager(context);
        }
        return instance;
    }

    /**
     * Constructor
     * @param context
     */
    private ReachabilityManager(Context context) {
        mManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    /**
     * Determina el tipo de conexión
     * @param hostName
     */
    public boolean isHostReachable(String hostName) {
        try {
            //Get int value from name
            byte[] ipv4 = InetAddress.getByName(hostName).getAddress();
            int address = ipv4[0];
            address += ipv4[1] << 8;
            address += ipv4[2] << 16;
            address += ipv4[3] << 24;
            return isHostReachable(address);
        } catch (UnknownHostException e) {
            return false;
        }
    }


    public boolean isHostReachable(int hostAddress) {
            || mManager.requestRouteToHost(ConnectivityManager.TYPE_MOBILE, hostAddress));
        try{
            java.lang.reflect.Method method = mManager.getClass().getMethod("requestRouteToHost", int.class, int.class);
            System.out.println("uwu");
            return ((boolean) method.invoke(ConnectivityManager.TYPE_WIFI, hostAddress)
                    || (boolean) method.invoke(ConnectivityManager.TYPE_MOBILE, hostAddress));
        } catch (Exception e) {
            return false;
        }

    }

    /**
     * Para verificar la conectividad
     */
    public boolean isNetworkReachable() {
        NetworkInfo current = mManager.getActiveNetworkInfo();
        if (current == null) {
            return false;
        }
        return (current.getState() == NetworkInfo.State.CONNECTED);
    }
}
