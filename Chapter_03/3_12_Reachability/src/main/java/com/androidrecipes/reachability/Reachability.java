package com.androidrecipes.reachability;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

/**
 * Problema: La aplicación necesita estar pendiente de los cambios de la conexión de network
 * Solución: Usaremos ConnectivityManager que provee la información del tipo de conexión
 */
public class Reachability extends Activity {

    ReachabilityManager mReach;

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mReach = ReachabilityManager.getInstance(this);
        Toast.makeText(this, "Network " + mReach.isNetworkReachable() + "\nGoogle " + mReach.isHostReachable(0xD155E368), Toast.LENGTH_SHORT).show();
    }
}