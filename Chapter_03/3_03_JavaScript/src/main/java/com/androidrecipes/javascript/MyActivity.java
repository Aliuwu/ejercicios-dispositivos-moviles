package com.androidrecipes.javascript;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

/**
 * Problema: La aplicación necesita acceder a la dirección HTML del contenido que se despliega en WebView
 * Solución: Crearemos una interfaz de JavaScript para crear un puente o enlace del WebView al código de la aplicación
 */
@TargetApi(19)
public class MyActivity extends Activity {
    String msj;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WebView webview = new WebView(this);
        //Habilitamos Javascript
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(mClient);
        //Agregamos la interfaz a la vista
        webview.addJavascriptInterface(new MyJavaScriptInterface(), "BRIDGE");

        setContentView(webview);

        webview.loadUrl("file:///android_asset/MyFormulario.html");
    }

    private static final String JS_SETELEMENT =
            "javascript:document.getElementById('%s').value='%s'";
    private static final String JS_GETELEMENT =
            "javascript:window.BRIDGE.storeElement('%s',document.getElementById('%s').value)";
    private static final String ELEMENTID = "nombres";
    private static final String ELEMENTID1 = "direccions";
    private static final String ELEMENTID2 = "emailAddress";

    private static final String[] ELEMENTID3 = {"nombres","direccions","emailAddress"};


    private WebViewClient mClient = new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            executeJavascript(view,
                    String.format(JS_GETELEMENT, ELEMENTID, ELEMENTID));
            executeJavascript(view,
                    String.format(JS_GETELEMENT, ELEMENTID1, ELEMENTID1));
            executeJavascript(view,
                    String.format(JS_GETELEMENT, ELEMENTID2, ELEMENTID2));

            return false;
        }

        /**
         * Cuando la página se carga, se inserta la dirección en la página usando JavaScript
         * @param view
         * @param url
         */
        @Override
        public void onPageFinished(WebView view, String url) {
            msj="";
            SharedPreferences prefs = getPreferences(Activity.MODE_PRIVATE);
            executeJavascript(view,
                    String.format(JS_SETELEMENT, ELEMENTID, prefs.getString(ELEMENTID, "")));
            executeJavascript(view,
                    String.format(JS_SETELEMENT, ELEMENTID1, prefs.getString(ELEMENTID1, "")));
            executeJavascript(view,
                    String.format(JS_SETELEMENT, ELEMENTID2, prefs.getString(ELEMENTID2, "")));
        }
    };

    private void executeJavascript(WebView view, String script) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            view.evaluateJavascript(script, null);
        } else {
            view.loadUrl(script);
        }
    }

    /**
     * Almacenar un elemento en preferencias
     */
    private class MyJavaScriptInterface {
        @JavascriptInterface
        public void storeElement(String id, String element) {
            SharedPreferences.Editor edit = getPreferences(Activity.MODE_PRIVATE).edit();
            edit.putString(id, element);
            msj=msj+element+"\n";
            edit.commit();
            if (!TextUtils.isEmpty(element)) {
                Toast.makeText(MyActivity.this, msj, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
