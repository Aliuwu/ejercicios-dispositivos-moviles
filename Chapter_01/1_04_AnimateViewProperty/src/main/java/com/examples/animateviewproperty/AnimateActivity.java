package com.examples.animateviewproperty;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
/**
 * Problema: La aplicación necesita animar un objeto de vista, ya sea como una transición o efecto
 * Solución: Necesitamos una instancia de ObjectAnimator como ViewPropertyAnimator
 */
public class AnimateActivity extends Activity implements View.OnClickListener {

    /**
     * Vista para animar
     */
    private View mViewToAnimate;

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        /**
         * Botón para iniciar la animación
         */
        Button button = (Button) findViewById(R.id.toggleButton);
        button.setOnClickListener(this);

        mViewToAnimate = findViewById(R.id.theView);
    }

    /**
     * Inicializa la animación
     * @param v
     */
    @Override
    public void onClick(View v) {
        if (mViewToAnimate.getAlpha() > 0f) {
            //If the view is visible already, slide it out to the right
            mViewToAnimate.animate().alpha(0f).translationX(500f);
        } else {
            //If the view is hidden, do a fade-in in-place
            //Property Animations actually modify the view, so
            // we have to reset the view's location first
            mViewToAnimate.setTranslationX(0f);
            mViewToAnimate.animate().alpha(1f);
        }
    }
}
