package com.examples.cardviewlist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Problema: La aplicación necesita usar CardView para mostrar items en un RecyclerView
 * Solución: Usaremos las librerías de CardView, RecyclerView y el layout PercentRelativeLayout para que las imágenes aparezcan dentro de cada carta
 */
public class CardViewActivity extends AppCompatActivity {

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RecyclerView recyclerView = new RecyclerView(this);
        setContentView(recyclerView);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new CardViewAdapter(this));
    }

}