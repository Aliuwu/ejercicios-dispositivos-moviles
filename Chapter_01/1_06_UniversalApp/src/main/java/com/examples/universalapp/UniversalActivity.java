package com.examples.universalapp;

import android.app.Activity;
import android.os.Bundle;

/**
 * Problema: La aplicación debe ser universal, funcionar en diferentes tamaños de pantalla y orientaciones de pantalla, se debe proveer recursos de layout para cada uno de estos casos
 * Solución: Construir varios archivos de layout y usar calificadores de recursos para dejar que Android elija el más apropiado
 */
public class UniversalActivity extends Activity {

    /**
     * Inicializa la actividad donde junta los layouts y deja que Android escoja el más apropiado
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
}