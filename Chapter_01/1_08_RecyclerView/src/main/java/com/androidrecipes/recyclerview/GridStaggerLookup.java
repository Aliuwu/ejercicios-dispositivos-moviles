package com.androidrecipes.recyclerview;

import android.support.v7.widget.GridLayoutManager;

public class GridStaggerLookup extends GridLayoutManager.SpanSizeLookup {

    /**
     * Obtiene el espacio que ocupa un item en una posición específica
     * @param position
     */
    @Override
    public int getSpanSize(int position) {
        return (position % 3 == 0 ? 2 : 1);
    }
}
