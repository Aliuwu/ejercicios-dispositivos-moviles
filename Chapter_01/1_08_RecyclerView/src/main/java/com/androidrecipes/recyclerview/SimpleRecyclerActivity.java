package com.androidrecipes.recyclerview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

/**
 * Problema: Se tiene una gran colección de datos que se desea prensentar de una manera única, en lugar de mostrarla en una scroll list
 * Solución: Usaremos RecyclerView que se encuentra en la librería Supoort
 */
public class SimpleRecyclerActivity extends AppCompatActivity implements
        SimpleItemAdapter.OnItemClickListener {

    private RecyclerView mRecyclerView;//Recycler
    private SimpleItemAdapter mAdapter;//Adapatador para el Recycler

    /* Administradores de layout */
    private LinearLayoutManager mHorizontalManager;
    private LinearLayoutManager mVerticalManager;
    private GridLayoutManager mVerticalGridManager;
    private GridLayoutManager mHorizontalGridManager;

    /* Decoraciones */
    private ConnectorDecoration mConnectors;

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Creamos un RecyclerView
        mRecyclerView = new RecyclerView(this);
        //Creamos los cuatro layouts que el usuario podrá escoger
        mHorizontalManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mVerticalManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mVerticalGridManager = new GridLayoutManager(this,
                2, /* Número de columnas */
                LinearLayoutManager.VERTICAL, /* Orientar la malla de forma vertical */
                false);
        mHorizontalGridManager = new GridLayoutManager(this,
                3, /* Número de filas */
                LinearLayoutManager.HORIZONTAL, /* Orientar la malla de forma horizontal */
                false);

        //Conector de las decoraciones para malla vertical
        mConnectors = new ConnectorDecoration(this);

        //Distribución para la malla vertical
        mVerticalGridManager.setSpanSizeLookup(new GridStaggerLookup());
        //Adaptador para el RecyclerView
        mAdapter = new SimpleItemAdapter(this);
        mAdapter.setOnItemClickListener(this);
        mRecyclerView.setAdapter(mAdapter);

        //Aplica márgenes de decoración
        mRecyclerView.addItemDecoration(new InsetDecoration(this));

        //Layout vertical por defecto
        selectLayoutManager(R.id.action_vertical);
        setContentView(mRecyclerView);
    }

    /**
     * Método para la creación de un menú con las 4 opciones de layout
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.layout_options, menu);
        return true;
    }

    /**
     * Obtiene el id del administrador de layout seleccionado
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return selectLayoutManager(item.getItemId());
    }

    /**
     * Para elegir un Administrador de layouts
     * @param id
     * @return
     */
    private boolean selectLayoutManager(int id) {
        switch (id) {
            case R.id.action_vertical:
                mRecyclerView.setLayoutManager(mVerticalManager);
                mRecyclerView.removeItemDecoration(mConnectors);
                return true;
            case R.id.action_horizontal:
                mRecyclerView.setLayoutManager(mHorizontalManager);
                mRecyclerView.removeItemDecoration(mConnectors);
                return true;
            case R.id.action_grid_vertical:
                mRecyclerView.setLayoutManager(mVerticalGridManager);
                mRecyclerView.addItemDecoration(mConnectors);
                return true;
            case R.id.action_grid_horizontal:
                mRecyclerView.setLayoutManager(mHorizontalGridManager);
                mRecyclerView.removeItemDecoration(mConnectors);
                return true;
            case R.id.action_add_item:
                //Inserta un nuevo item
                mAdapter.insertItemAtIndex("Android Recipes", 1);
                return true;
            case R.id.action_remove_item:
                //Remueve el primer item
                mAdapter.removeItemAtIndex(1);
                return true;
            default:
                return false;
        }
    }

    /**
     * Métodos OnItemClickListener
     */
    @Override
    public void onItemClick(SimpleItemAdapter.ItemHolder item, int position) {
        Toast.makeText(this, item.getSummary(), Toast.LENGTH_SHORT).show();
    }
}
