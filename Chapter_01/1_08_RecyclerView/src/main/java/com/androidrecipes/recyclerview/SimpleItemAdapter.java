package com.androidrecipes.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SimpleItemAdapter extends RecyclerView.Adapter<SimpleItemAdapter.ItemHolder> {

    public interface OnItemClickListener {
        public void onItemClick(ItemHolder item, int position);
    }

    /**
     * Arreglo con los items posibbles
     */
    private static final String[] ITEMS = {
            "Apples", "Oranges", "Bananas", "Mangos",
            "Carrots", "Peas", "Broccoli",
            "Pork", "Chicken", "Beef", "Lamb"
    };
    private List<String> mItems;

    private OnItemClickListener mOnItemClickListener;
    private LayoutInflater mLayoutInflater;

    /**
     * Crea una lista estática de items por defecto
     * @param context
     */
    public SimpleItemAdapter(Context context) {
        mLayoutInflater = LayoutInflater.from(context);
        mItems = new ArrayList<String>();
        mItems.addAll(Arrays.asList(ITEMS));
        mItems.addAll(Arrays.asList(ITEMS));
    }

    /**
     *Para representar un item
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mLayoutInflater.inflate(R.layout.collection_item, parent, false);

        return new ItemHolder(itemView, this);
    }

    /**
     * Muestra los datos de una posición específica
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {
        holder.setTitle("Item #" + (position + 1));
        holder.setSummary(mItems.get(position));
    }

    /**
     * Devuelvo el tamaño de la lista de items
     * @return
     */
    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public OnItemClickListener getOnItemClickListener() {
        return mOnItemClickListener;
    }

    /**
     * Asigna un OnItemListener
     * @param listener
     */
    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }


    /**
     * Insertar un item en una posición específica
     * @param item
     * @param position
     */
    public void insertItemAtIndex(String item, int position) {
        mItems.add(position, item);
        notifyItemInserted(position);
    }

    /**
     * Para remover un item en un índice específico
     * @param position
     */
    public void removeItemAtIndex(int position) {
        if (position >= mItems.size()) return;

        mItems.remove(position);
        notifyItemRemoved(position);
    }

    /**
     * Implementación de ViewHolder para la vista de items
     */
    public static class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private SimpleItemAdapter mParent;
        private TextView mTitleView, mSummaryView;

        public ItemHolder(View itemView, SimpleItemAdapter parent) {
            super(itemView);
            itemView.setOnClickListener(this);
            mParent = parent;

            mTitleView = (TextView) itemView.findViewById(R.id.text_title);
            mSummaryView = (TextView) itemView.findViewById(R.id.text_summary);
        }

        public void setTitle(CharSequence title) {
            mTitleView.setText(title);
        }

        public void setSummary(CharSequence summary) {
            mSummaryView.setText(summary);
        }

        public CharSequence getSummary() {
            return mSummaryView.getText();
        }

        @Override
        public void onClick(View v) {
            final OnItemClickListener listener = mParent.getOnItemClickListener();
            if (listener != null) {
                listener.onItemClick(this, getPosition());
            }
        }
    }
}
