package com.examples.statictransforms;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Transformation;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

public class PerspectiveScrollContentView extends LinearLayout {


    private static final float SCALE_FACTOR = 0.7f;
    private static final float ANCHOR_X = 0.5f;
    private static final float ANCHOR_Y = 1.0f;

    public PerspectiveScrollContentView(Context context) {
        super(context);
        init();
    }

    public PerspectiveScrollContentView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PerspectiveScrollContentView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    /**
     * Habilita las transformaciones estáticas
     */
    private void init() {
        setStaticTransformationsEnabled(true);
    }

    /**
     * Permite calcular la posición actual de cualquier vista en las coordenadas de la pantalla
     * @param view
     */
    private int getViewCenter(View view) {
        int[] childCoords = new int[2];
        view.getLocationOnScreen(childCoords);
        int childCenter = childCoords[0] + (view.getWidth() / 2);

        return childCenter;
    }

    @Override
    protected boolean getChildStaticTransformation(View child, Transformation t) {
        HorizontalScrollView scrollView = null;
        if (getParent() instanceof HorizontalScrollView) {
            scrollView = (HorizontalScrollView) getParent();
        }
        if (scrollView == null) {
            return false;
        }

        int childCenter = getViewCenter(child);
        int viewCenter = getViewCenter(scrollView);
        //Calcula la diferencia entre entre el centro del componente hijo y el del padre para determinar el factor de escala
        float delta = Math.min(1.0f, Math.abs(childCenter - viewCenter)
                / (float) viewCenter);
        float scale = Math.max(0.4f, 1.0f - (SCALE_FACTOR * delta));
        float xTrans = child.getWidth() * ANCHOR_X;
        float yTrans = child.getHeight() * ANCHOR_Y;

        //Elimina cualquier tipo de transformación
        t.clear();
        //Asigna la transformación para la vista del componente hijo
        t.getMatrix().setScale(scale, scale, xTrans, yTrans);

        return true;
    }
}
