package com.examples.statictransforms;

import android.app.Activity;
import android.os.Bundle;

/**
 * Problema: La aplicación necesita transformar de forma dinámica como se ven las vistas para añadir algunos efectos visuales como perspectiva
 * Solución: Usaremos el método setStaticTransformationsEnabled(true) durante la incialización
 */
public class MainActivity extends Activity {

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
}
