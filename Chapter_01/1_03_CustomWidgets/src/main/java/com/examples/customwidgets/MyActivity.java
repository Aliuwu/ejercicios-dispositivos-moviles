package com.examples.customwidgets;

import android.app.Activity;
import android.os.Bundle;

/**
 * Problema: La aplicación necesita elementos de vista para desplegar información e interactuar con el usuario
 * Solución: Crearemos las interfaces a partir de un archivo xml (layout)
 */
public class MyActivity extends Activity {

    /**
     * Inicializamos la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
}