package com.androidrecipes.appstyles;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class StylesActivity extends AppCompatActivity {

    /**
     * Problema: Se quiere crear una apariencia consistente para que la aplicación se vea bien en todas las versiones de Android en las que se ejecute
     * Solución: Vamos a usar styles, colecciones de características personalizadas para la apariencia como el tamaño del texto o el color de fondo, a través de un xml
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_styled);
    }
}
