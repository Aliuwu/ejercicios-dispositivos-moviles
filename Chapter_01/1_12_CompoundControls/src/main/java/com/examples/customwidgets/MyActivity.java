package com.examples.customwidgets;

import android.app.Activity;
import android.os.Bundle;

/**
 * Problema: Se necesita crear un widget personalizado que sea una colección de los elementos existentes
 * Solución: Usaremos un objeto de ViewGroup
 */
public class MyActivity extends Activity {

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
}