package com.examples.customwidgets;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class TextImageButton extends FrameLayout {

    private ImageView imageView;
    private TextView textView;

    /* Constructores */
    public TextImageButton(Context context) {
        this(context, null);
    }

    public TextImageButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }


    public TextImageButton(Context context, AttributeSet attrs, int defaultStyle) {
        //Inicializa el layout con el estilo del botón del sistema para que los atributos del background coincidan
        super(context, attrs, android.R.attr.buttonStyle);
        imageView = new ImageView(context, attrs, defaultStyle);
        textView = new TextView(context, attrs, defaultStyle);
        //crea los parámetros del layout
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT,
                Gravity.CENTER);
        //Añade las vistas
        this.addView(imageView, params);
        this.addView(textView, params);

        //si la imagen está presenta cambia al modo de imagen
        if (imageView.getDrawable() != null) {
            textView.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
        }
    }


    /**
     * Asigna el texto
     * @param text
     */
    public void setText(CharSequence text) {
        textView.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.GONE);
        textView.setText(text);
    }

    /**
     * Asigna la imagen
     * @param resId
     */
    public void setImageResource(int resId) {
        textView.setVisibility(View.GONE);
        imageView.setVisibility(View.VISIBLE);
        imageView.setImageResource(resId);
    }

    /**
     * Asigna la imagen de drawable
     * @param drawable
     */
    public void setImageDrawable(Drawable drawable) {
        textView.setVisibility(View.GONE);
        imageView.setVisibility(View.VISIBLE);
        imageView.setImageDrawable(drawable);
    }
}

