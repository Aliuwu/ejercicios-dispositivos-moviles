package com.examples.transitionanimations;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Problema: La aplicación necesita personalizar las animaciones de transición que ocurren cuando se mueve de una actividad a otra entre fragments
 * Solución: Usaremos la API overridePendingTransition() para una solo caso,
 *           para modificar la transición entre fragments usaremos los métodos onCreateAnimation() o onCreateAnimator()
 */
public class MainActivity extends Activity implements View.OnClickListener {

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button b = new Button(this);
        b.setText("Click");
        b.setOnClickListener(this);
        setContentView(b);
    }

    /**
     * Empeiza la actividad en la posición i
     * @param v
     */
    @Override
    public void onClick(View v) {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    /**
     * Es llamado cuando se detecta que el usuario presiona la tecla back o atrás
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
