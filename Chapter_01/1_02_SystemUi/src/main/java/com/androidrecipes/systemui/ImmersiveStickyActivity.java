package com.androidrecipes.systemui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class ImmersiveStickyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

    }

    public void onToggleClick(View v) {
        //La interfaz de usuario se ocultará solo con un "tap", el sistema permitirá que reaparezca cuando la acción sea finalizada
        v.setSystemUiVisibility(
                /* Para no cambiar el diseño cuando se redimensione la ventana al ocultar los controles
                 */
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                /* Esconde la barra de notificaciones
                 */
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                /* Esconde los controles de la pantalla
                 */
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                /* Los controles permanecen ocultos hasta que la acción termine
                 */
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}
