package com.androidrecipes.systemui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;

public class FullActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Para esconder la barra de notificaciones
        supportRequestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.main);

    }

    public void onToggleClick(View v) {
        //Solo necesitamos un "tap" para cambiar el modo
        v.setSystemUiVisibility(
                /* Para que no cambie el diseño cuando se redimensione la ventana
                 */
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                /* Oculta la barra de notificaciones
                 */
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                /* Oculta los controles de la pantalla
                 */
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}
