package com.androidrecipes.systemui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class HideActivity extends AppCompatActivity {

    /**
     * Inicializa la actividad Hide
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

    }

    public void onToggleClick(View v) {
        //Solo se dará un tap para cambiar el modo
        //Se cambiará automáticamente al modo default cuando se termine la acción
        v.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}
