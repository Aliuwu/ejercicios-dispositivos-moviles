package com.androidrecipes.systemui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Problema: La aplicación necesita remover cualquier decoración del sistema como la barra de notificaciones y algunos botones de navegación
 * Solución: Esconder temporalmente los componenentes de interfaz de usuario del sistema cuando el contenido de la aplicación se encuentre visible
 */
public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    /**
     * Arreglo que guarda las opciones de los modos que usará la aplicación
     */
    private String[] mActivities = new String[]{
            "Dark Mode", "Hide Navigation", "Fullscreen Mode",
            "Immersive Mode", "Immersive Sticky Mode"};
    /**
     * Adaptador para el ListView
     */
    private ArrayAdapter<String> mAdapter;

    /**
     * Inicializa la actividad y sus componentes
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ListView list = new ListView(this);

        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mActivities);
        list.setAdapter(mAdapter);
        list.setOnItemClickListener(this);
        setContentView(list);
    }

    /**
     * Permite escoger entre cualquiera de los modos definidos en el arreglo mActivities
     * @param parent
     * @param v
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        Intent i;
        switch (position) {
            case 0:
                i = new Intent(this, DarkActivity.class);
                break;
            case 1:
                i = new Intent(this, HideActivity.class);
                break;
            case 2:
                i = new Intent(this, FullActivity.class);
                break;
            case 3:
                i = new Intent(this, ImmersiveActivity.class);
                break;
            case 4:
                i = new Intent(this, ImmersiveStickyActivity.class);
                break;
            default:
                return;
        }
        startActivity(i);
    }
}
