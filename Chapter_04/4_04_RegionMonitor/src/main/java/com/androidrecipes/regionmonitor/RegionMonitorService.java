package com.androidrecipes.regionmonitor;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

public class RegionMonitorService extends Service {
    //Acció única para identificar solicitudes de inicio vs eventos
    public static final String ACTION_INIT =
            "com.androidrecipes.regionmonitor.ACTION_INIT";
    private static final String TAG = "RegionMonitorService";
    private static final int NOTE_ID = 100;
    private NotificationManager mNoteManager;

    /**
     * Inicializa la clase
     */
    @Override
    public void onCreate() {
        super.onCreate();
        mNoteManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        //Notificaciones del sistema
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setContentTitle("Geofence Service");
        builder.setContentText("Waiting for transition...");
        builder.setOngoing(true);

        Notification note = builder.build();
        mNoteManager.notify(NOTE_ID, note);
    }

    /**
     * Inicia el servicio
     * @param intent
     * @param flags
     * @param startId
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (ACTION_INIT.equals(intent.getAction())) {
            return START_NOT_STICKY;
        }

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            //Mensajes de error
            Log.w(TAG, "Error monitoring region: "
                    + geofencingEvent.getErrorCode());
        } else {
            //Actualiza las notificaciones
            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(this);
            builder.setSmallIcon(R.drawable.ic_launcher);
            builder.setDefaults(Notification.DEFAULT_SOUND
                    | Notification.DEFAULT_LIGHTS);
            builder.setAutoCancel(true);

            int transitionType = geofencingEvent.getGeofenceTransition();

            //Verifica si se entró o salió de la región
            if (transitionType == Geofence.GEOFENCE_TRANSITION_ENTER) {
                builder.setContentTitle("Geofence Transition");
                builder.setContentText("Entered your Geofence");
            } else if (transitionType == Geofence.GEOFENCE_TRANSITION_EXIT) {
                builder.setContentTitle("Geofence Transition");
                builder.setContentText("Exited your Geofence");
            }

            Notification note = builder.build();
            mNoteManager.notify(NOTE_ID, note);
        }

        return START_NOT_STICKY;
    }

    /**
     * El servicio muere, se cancela la notificación
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mNoteManager.cancel(NOTE_ID);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
