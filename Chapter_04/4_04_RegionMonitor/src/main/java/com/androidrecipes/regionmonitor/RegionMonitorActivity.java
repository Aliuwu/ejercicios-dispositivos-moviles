package com.androidrecipes.regionmonitor;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.icu.util.ICUUncheckedIOException;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Problema: La aplicación necesita proporcionar información contextual a los usuarios cuando entren o salgan de áreas específicas
 * Solución: Usaremos las herramientas de georeferencia de Google Play Services
 */
public class RegionMonitorActivity extends Activity implements
        OnSeekBarChangeListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {
    private static final String TAG = "RegionMonitorActivity";
    //Identificador único para geofence
    private static final String FENCE_ID = "com.androidrecipes.FENCE";
    private static final int REQUEST_CODE_PERMISSIONS = 10;

    private SeekBar mRadiusSlider;
    private TextView mStatusText, mRadiusText;

    private Geofence mCurrentFence;
    private PendingIntent mCallbackIntent;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Conecta las conexiones de la interfaz de usuario
        mStatusText = (TextView) findViewById(R.id.status);
        mRadiusText = (TextView) findViewById(R.id.radius_text);
        mRadiusSlider = (SeekBar) findViewById(R.id.radius);
        mRadiusSlider.setOnSeekBarChangeListener(this);
        mRadiusText.setText(mRadiusSlider.getProgress() + " meters");

        //Comprueba si Google Play Services está actualizado.
        int errorCode = GoogleApiAvailability.getInstance()
                .isGooglePlayServicesAvailable(this);
        switch (errorCode) {
            case ConnectionResult.SUCCESS:
                break;
            default:
                DialogInterface.OnCancelListener onCancelListener
                        = new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                };
                GoogleApiAvailability.getInstance()
                        .showErrorDialogFragment(this, errorCode, 10,
                                onCancelListener);
                return;
        }

        Intent serviceIntent = new Intent(this, RegionMonitorService.class);
        mCallbackIntent = PendingIntent.getService(this, 0, serviceIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        //Crea un cliente para Google Services
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    /**
     * Para conectar con todos los servicios
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_CODE_PERMISSIONS);
            return;
        }

        mGoogleApiClient.connect();
    }

    /**
     * Desconecta cuando no está en primer plano
     */
    @Override
    protected void onPause() {
        super.onPause();
        mGoogleApiClient.disconnect();
    }

    /**
     * Para crear Geofence
     * @param v
     */
    @SuppressWarnings("MissingPermission")
    public void onSetGeofenceClick(View v) {
        //Obtiene la última ubicación
        Location current = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);
        System.out.println("t "+current);
        int radius = mRadiusSlider.getProgress();

        Geofence.Builder builder = new Geofence.Builder();
        mCurrentFence = builder
                //Id único de geofence
                .setRequestId(FENCE_ID)
                //Tamaño y ubicación
                .setCircularRegion(
                        current.getLatitude(),
                        current.getLongitude(),
                        radius)

                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER
                        | Geofence.GEOFENCE_TRANSITION_EXIT
                        | Geofence.GEOFENCE_TRANSITION_DWELL)
                .setLoiteringDelay(30)

                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .build();

        String text = "";
        if (current.getLatitude()==0 || current.getLongitude()==0 ){
            text="Ingrese la georeferencia";
        }else{
            text=String.format(Locale.getDefault(),
                    "Geofence set at %.3f, %.3f",
                    current.getLatitude(), current.getLongitude());
        }
        mStatusText.setText(text);
        System.out.println(current.getLatitude()+" "+current.getLongitude());


    }

    /**
     * Empieza el monitoreo
     * @param v
     */
    @SuppressWarnings("MissingPermission")
    public void onStartMonitorClick(View v) {
        if (mCurrentFence == null) {
            Toast.makeText(this, "Geofence Not Yet Set",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        ArrayList<Geofence> geofences = new ArrayList<>();
        geofences.add(mCurrentFence);
        GeofencingRequest geofencingRequest
                = new GeofencingRequest.Builder()
                .addGeofences(geofences)
                .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_DWELL)
                .build();
        LocationServices.GeofencingApi
                .addGeofences(mGoogleApiClient, geofencingRequest,
                        mCallbackIntent)
        .setResultCallback(this);
    }

    /**
     * Detiene el monitoreo
     * @param v
     */
    public void onStopMonitorClick(View v) {
        LocationServices.GeofencingApi
                .removeGeofences(mGoogleApiClient, mCallbackIntent);
    }

    /**
     * SeekBar Callbacks
     */

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress,
                                  boolean fromUser) {
        mRadiusText.setText(seekBar.getProgress() + " meters");
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.v(TAG, "Google Services Connected");
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.w(TAG, "Google Services Connection Failure");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions,
                grantResults);

        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            for (int i = 0; i < grantResults.length; i++) {
                int grantResult = grantResults[i];
                String permission = permissions[i];
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "Permission " + permission
                            + " is required for this application to work.");
                    finish();
                    return;
                }
            }
        }
    }

    @Override
    public void onResult(@NonNull Status status) {
        if (status.isSuccess()) {
            Log.d(TAG, "Geofence operation successful");
        } else {
            Log.d(TAG, "Geofence operation failed: " + status.getStatusMessage());
        }
    }
}
