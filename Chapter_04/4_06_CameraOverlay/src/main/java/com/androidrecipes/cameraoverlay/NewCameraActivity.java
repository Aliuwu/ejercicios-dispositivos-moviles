package com.androidrecipes.cameraoverlay;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * Problema: La aplicación necesita un acceso más directo a la cámara
 * Solución: Usaremos  APIs que pueden ayudar a acceder de manera más directa a la cámara
 */
public class NewCameraActivity extends AppCompatActivity {

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_camera);
    }
}
