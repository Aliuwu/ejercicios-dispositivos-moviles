1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.androidrecipes.mapper.debug"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="14"
8-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml
9        android:targetSdkVersion="29" />
9-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml
10
11    <!-- Required to display the user's location on the map -->
12    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
12-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:8:5-81
12-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:8:22-78
13    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
13-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:9:5-78
13-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:9:22-76
14    <!-- Permissions Required to Display a Map -->
15    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
15-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:9:5-78
15-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:9:22-76
16    <uses-permission android:name="android.permission.INTERNET" />
16-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:12:5-66
16-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:12:22-64
17    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
17-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:13:5-78
17-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:13:22-76
18    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
18-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:14:5-80
18-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:14:22-78
19    <uses-permission android:name="com.google.android.providers.gfs.permission.READ_GSERVICES" />
19-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:15:5-97
19-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:15:22-95
20
21    <!-- Maps v2 requires OpenGL ES 2.0 -->
22    <uses-feature
22-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:20:5-22:34
23        android:glEsVersion="0x00020000"
23-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:21:9-41
24        android:required="true" />
24-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:22:9-32
25
26    <!-- Optional permission for Analytics to run. -->
27    <uses-permission android:name="android.permission.WAKE_LOCK" /> <!-- Permissions required for GCM -->
27-->[com.google.android.gms:play-services-analytics:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\88c1b2f426cdec8c1535bab7c32f4d70\play-services-analytics-9.2.1\AndroidManifest.xml:25:5-67
27-->[com.google.android.gms:play-services-analytics:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\88c1b2f426cdec8c1535bab7c32f4d70\play-services-analytics-9.2.1\AndroidManifest.xml:25:22-65
28    <uses-permission android:name="com.google.android.c2dm.permission.RECEIVE" />
28-->[com.google.android.gms:play-services-gcm:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\d708cc9d6fc2028dddfd00ffeb0c5eeb\play-services-gcm-9.2.1\AndroidManifest.xml:21:5-82
28-->[com.google.android.gms:play-services-gcm:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\d708cc9d6fc2028dddfd00ffeb0c5eeb\play-services-gcm-9.2.1\AndroidManifest.xml:21:22-79
29
30    <permission
30-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:25:5-26:47
31        android:name="com.androidrecipes.mapper.debug.permission.C2D_MESSAGE"
31-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:25:17-71
32        android:protectionLevel="signature" />
32-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:26:9-44
33
34    <uses-permission android:name="com.androidrecipes.mapper.debug.permission.C2D_MESSAGE" />
34-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:28:5-79
34-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:28:22-76
35
36    <application
36-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:24:5-70:19
37        android:name="android.support.multidex.MultiDexApplication"
38        android:debuggable="true"
39        android:icon="@drawable/ic_launcher"
39-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:26:9-45
40        android:label="@string/app_name"
40-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:27:9-41
41        android:testOnly="true"
42        android:theme="@style/AppTheme" >
42-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:28:9-40
43        <activity
43-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:29:9-36:20
44            android:name="com.androidrecipes.mapper.BasicMapActivity"
44-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:30:13-70
45            android:label="Basic Map" >
45-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:31:13-38
46            <intent-filter>
46-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:32:13-35:29
47                <action android:name="android.intent.action.MAIN" />
47-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:33:17-68
47-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:33:25-66
48
49                <category android:name="android.intent.category.LAUNCHER" />
49-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:34:17-76
49-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:34:27-74
50            </intent-filter>
51        </activity>
52        <activity
52-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:38:9-45:20
53            android:name="com.androidrecipes.mapper.ShapeMapActivity"
53-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:39:13-70
54            android:label="Shape Map" >
54-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:40:13-38
55            <intent-filter>
55-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:32:13-35:29
56                <action android:name="android.intent.action.MAIN" />
56-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:33:17-68
56-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:33:25-66
57
58                <category android:name="android.intent.category.LAUNCHER" />
58-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:34:17-76
58-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:34:27-74
59            </intent-filter>
60        </activity>
61        <activity
61-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:47:9-55:20
62            android:name="com.androidrecipes.mapper.MarkerMapActivity"
62-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:48:13-71
63            android:label="Marker Map" >
63-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:49:13-39
64            <intent-filter>
64-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:32:13-35:29
65                <action android:name="android.intent.action.MAIN" />
65-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:33:17-68
65-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:33:25-66
66
67                <category android:name="android.intent.category.LAUNCHER" />
67-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:34:17-76
67-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:34:27-74
68            </intent-filter>
69        </activity>
70
71        <!-- Required Metadata Elements for Maps -->
72        <meta-data
72-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:58:9-60:70
73            android:name="com.google.android.maps.v2.API_KEY"
73-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:59:13-62
74            android:value="AIzaSyBRzTWGqpNxOP3S5Ysk0aiPsVbaO0Muusw" />
74-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:60:13-68
75        <meta-data
75-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:62:9-64:68
76            android:name="com.google.android.gms.version"
76-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:63:13-58
77            android:value="@integer/google_play_services_version" />
77-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:64:13-66
78
79        <uses-library
79-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:66:9-68:40
80            android:name="org.apache.http.legacy"
80-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:67:13-50
81            android:required="false" />
81-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_02-03_Mapper\src\main\AndroidManifest.xml:68:13-37
82
83        <!-- Include the AdActivity and InAppPurchaseActivity configChanges and themes. -->
84        <activity
84-->[com.google.android.gms:play-services-ads-lite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\b0bd198f64ba5796b5bb4af113f720ec\play-services-ads-lite-9.2.1\AndroidManifest.xml:26:9-29:64
85            android:name="com.google.android.gms.ads.AdActivity"
85-->[com.google.android.gms:play-services-ads-lite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\b0bd198f64ba5796b5bb4af113f720ec\play-services-ads-lite-9.2.1\AndroidManifest.xml:27:13-65
86            android:configChanges="keyboard|keyboardHidden|orientation|screenLayout|uiMode|screenSize|smallestScreenSize"
86-->[com.google.android.gms:play-services-ads-lite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\b0bd198f64ba5796b5bb4af113f720ec\play-services-ads-lite-9.2.1\AndroidManifest.xml:28:13-122
87            android:theme="@android:style/Theme.Translucent" />
87-->[com.google.android.gms:play-services-ads-lite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\b0bd198f64ba5796b5bb4af113f720ec\play-services-ads-lite-9.2.1\AndroidManifest.xml:29:13-61
88        <activity
88-->[com.google.android.gms:play-services-ads-lite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\b0bd198f64ba5796b5bb4af113f720ec\play-services-ads-lite-9.2.1\AndroidManifest.xml:30:9-31:52
89            android:name="com.google.android.gms.ads.purchase.InAppPurchaseActivity"
89-->[com.google.android.gms:play-services-ads-lite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\b0bd198f64ba5796b5bb4af113f720ec\play-services-ads-lite-9.2.1\AndroidManifest.xml:30:19-91
90            android:theme="@style/Theme.IAPTheme" />
90-->[com.google.android.gms:play-services-ads-lite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\b0bd198f64ba5796b5bb4af113f720ec\play-services-ads-lite-9.2.1\AndroidManifest.xml:31:13-50
91        <activity
91-->[com.google.android.gms:play-services-appinvite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\11fbf75f25d8968f9a0426837212aef1\play-services-appinvite-9.2.1\AndroidManifest.xml:21:9-29:20
92            android:name="com.google.android.gms.appinvite.PreviewActivity"
92-->[com.google.android.gms:play-services-appinvite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\11fbf75f25d8968f9a0426837212aef1\play-services-appinvite-9.2.1\AndroidManifest.xml:22:17-48
93            android:exported="true"
93-->[com.google.android.gms:play-services-appinvite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\11fbf75f25d8968f9a0426837212aef1\play-services-appinvite-9.2.1\AndroidManifest.xml:23:17-40
94            android:theme="@style/Theme.AppInvite.Preview" >
94-->[com.google.android.gms:play-services-appinvite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\11fbf75f25d8968f9a0426837212aef1\play-services-appinvite-9.2.1\AndroidManifest.xml:24:17-63
95            <intent-filter>
95-->[com.google.android.gms:play-services-appinvite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\11fbf75f25d8968f9a0426837212aef1\play-services-appinvite-9.2.1\AndroidManifest.xml:25:13-28:29
96                <action android:name="com.google.android.gms.appinvite.ACTION_PREVIEW" />
96-->[com.google.android.gms:play-services-appinvite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\11fbf75f25d8968f9a0426837212aef1\play-services-appinvite-9.2.1\AndroidManifest.xml:26:17-89
96-->[com.google.android.gms:play-services-appinvite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\11fbf75f25d8968f9a0426837212aef1\play-services-appinvite-9.2.1\AndroidManifest.xml:26:25-87
97
98                <category android:name="android.intent.category.DEFAULT" />
98-->[com.google.android.gms:play-services-appinvite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\11fbf75f25d8968f9a0426837212aef1\play-services-appinvite-9.2.1\AndroidManifest.xml:27:17-75
98-->[com.google.android.gms:play-services-appinvite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\11fbf75f25d8968f9a0426837212aef1\play-services-appinvite-9.2.1\AndroidManifest.xml:27:27-73
99            </intent-filter>
100        </activity>
101        <activity
101-->[com.google.android.gms:play-services-auth:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\9410201e81c1884fbc8a782015c944ad\play-services-auth-9.2.1\AndroidManifest.xml:21:9-24:46
102            android:name="com.google.android.gms.auth.api.signin.internal.SignInHubActivity"
102-->[com.google.android.gms:play-services-auth:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\9410201e81c1884fbc8a782015c944ad\play-services-auth-9.2.1\AndroidManifest.xml:21:19-68
103            android:excludeFromRecents="true"
103-->[com.google.android.gms:play-services-auth:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\9410201e81c1884fbc8a782015c944ad\play-services-auth-9.2.1\AndroidManifest.xml:23:19-52
104            android:exported="false"
104-->[com.google.android.gms:play-services-auth:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\9410201e81c1884fbc8a782015c944ad\play-services-auth-9.2.1\AndroidManifest.xml:24:19-43
105            android:theme="@android:style/Theme.Translucent.NoTitleBar" />
105-->[com.google.android.gms:play-services-auth:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\9410201e81c1884fbc8a782015c944ad\play-services-auth-9.2.1\AndroidManifest.xml:22:19-78
106        <!--
107Service handling Google Sign-In user revocation. For apps that do not integrate with
108            Google Sign-In, this service will never be started.
109        -->
110        <service
110-->[com.google.android.gms:play-services-auth:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\9410201e81c1884fbc8a782015c944ad\play-services-auth-9.2.1\AndroidManifest.xml:28:9-31:110
111            android:name="com.google.android.gms.auth.api.signin.RevocationBoundService"
111-->[com.google.android.gms:play-services-auth:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\9410201e81c1884fbc8a782015c944ad\play-services-auth-9.2.1\AndroidManifest.xml:29:13-58
112            android:exported="true"
112-->[com.google.android.gms:play-services-auth:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\9410201e81c1884fbc8a782015c944ad\play-services-auth-9.2.1\AndroidManifest.xml:30:13-36
113            android:permission="com.google.android.gms.auth.api.signin.permission.REVOCATION_NOTIFICATION" />
113-->[com.google.android.gms:play-services-auth:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\9410201e81c1884fbc8a782015c944ad\play-services-auth-9.2.1\AndroidManifest.xml:31:13-107
114
115        <receiver android:name="com.google.android.gms.cast.framework.media.MediaIntentReceiver" />
115-->[com.google.android.gms:play-services-cast-framework:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\dd14ee8bff8975bb00cdb2b2a641ffb0\play-services-cast-framework-9.2.1\AndroidManifest.xml:6:9-100
115-->[com.google.android.gms:play-services-cast-framework:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\dd14ee8bff8975bb00cdb2b2a641ffb0\play-services-cast-framework-9.2.1\AndroidManifest.xml:6:19-97
116
117        <service android:name="com.google.android.gms.cast.framework.media.MediaNotificationService" />
117-->[com.google.android.gms:play-services-cast-framework:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\dd14ee8bff8975bb00cdb2b2a641ffb0\play-services-cast-framework-9.2.1\AndroidManifest.xml:7:9-104
117-->[com.google.android.gms:play-services-cast-framework:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\dd14ee8bff8975bb00cdb2b2a641ffb0\play-services-cast-framework-9.2.1\AndroidManifest.xml:7:18-101
118        <service android:name="com.google.android.gms.cast.framework.ReconnectionService" />
118-->[com.google.android.gms:play-services-cast-framework:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\dd14ee8bff8975bb00cdb2b2a641ffb0\play-services-cast-framework-9.2.1\AndroidManifest.xml:8:9-92
118-->[com.google.android.gms:play-services-cast-framework:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\dd14ee8bff8975bb00cdb2b2a641ffb0\play-services-cast-framework-9.2.1\AndroidManifest.xml:8:18-90
119        <service
119-->[com.google.firebase:firebase-crash:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1fbe3f0cc3351386ca3d124f626dc3eb\firebase-crash-9.2.1\AndroidManifest.xml:26:9-27:50
120            android:name="com.google.firebase.crash.internal.service.FirebaseCrashReceiverService"
120-->[com.google.firebase:firebase-crash:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1fbe3f0cc3351386ca3d124f626dc3eb\firebase-crash-9.2.1\AndroidManifest.xml:26:18-79
121            android:process=":background_crash" />
121-->[com.google.firebase:firebase-crash:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1fbe3f0cc3351386ca3d124f626dc3eb\firebase-crash-9.2.1\AndroidManifest.xml:27:13-48
122        <service
122-->[com.google.firebase:firebase-crash:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1fbe3f0cc3351386ca3d124f626dc3eb\firebase-crash-9.2.1\AndroidManifest.xml:29:9-30:50
123            android:name="com.google.firebase.crash.internal.service.FirebaseCrashSenderService"
123-->[com.google.firebase:firebase-crash:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1fbe3f0cc3351386ca3d124f626dc3eb\firebase-crash-9.2.1\AndroidManifest.xml:29:18-77
124            android:process=":background_crash" />
124-->[com.google.firebase:firebase-crash:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1fbe3f0cc3351386ca3d124f626dc3eb\firebase-crash-9.2.1\AndroidManifest.xml:30:13-48
125
126        <activity
126-->[com.google.android.gms:play-services-base:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\bc476d6fc8d57e7ce974bfaabd002304\play-services-base-9.2.1\AndroidManifest.xml:20:9-22:45
127            android:name="com.google.android.gms.common.api.GoogleApiActivity"
127-->[com.google.android.gms:play-services-base:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\bc476d6fc8d57e7ce974bfaabd002304\play-services-base-9.2.1\AndroidManifest.xml:20:19-85
128            android:exported="false"
128-->[com.google.android.gms:play-services-base:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\bc476d6fc8d57e7ce974bfaabd002304\play-services-base-9.2.1\AndroidManifest.xml:22:19-43
129            android:theme="@android:style/Theme.Translucent.NoTitleBar" />
129-->[com.google.android.gms:play-services-base:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\bc476d6fc8d57e7ce974bfaabd002304\play-services-base-9.2.1\AndroidManifest.xml:21:19-78
130        <!--
131 FirebaseMessagingService performs security checks at runtime,
132             no need for explicit permissions despite exported="true"
133        -->
134        <service
134-->[com.google.firebase:firebase-messaging:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\bf6611c63e4b1039025b8366c7f6e645\firebase-messaging-9.2.1\AndroidManifest.xml:24:9-30:19
135            android:name="com.google.firebase.messaging.FirebaseMessagingService"
135-->[com.google.firebase:firebase-messaging:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\bf6611c63e4b1039025b8366c7f6e645\firebase-messaging-9.2.1\AndroidManifest.xml:25:13-82
136            android:exported="true" >
136-->[com.google.firebase:firebase-messaging:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\bf6611c63e4b1039025b8366c7f6e645\firebase-messaging-9.2.1\AndroidManifest.xml:26:13-36
137            <intent-filter android:priority="-500" >
137-->[com.google.firebase:firebase-messaging:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\bf6611c63e4b1039025b8366c7f6e645\firebase-messaging-9.2.1\AndroidManifest.xml:27:13-29:29
137-->[com.google.firebase:firebase-messaging:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\bf6611c63e4b1039025b8366c7f6e645\firebase-messaging-9.2.1\AndroidManifest.xml:27:28-51
138                <action android:name="com.google.firebase.MESSAGING_EVENT" />
138-->[com.google.firebase:firebase-messaging:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\bf6611c63e4b1039025b8366c7f6e645\firebase-messaging-9.2.1\AndroidManifest.xml:28:17-78
138-->[com.google.firebase:firebase-messaging:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\bf6611c63e4b1039025b8366c7f6e645\firebase-messaging-9.2.1\AndroidManifest.xml:28:25-75
139            </intent-filter>
140        </service>
141        <service
141-->[com.google.android.gms:play-services-tagmanager-api:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1e454b2e27696a8d91b270823ea27fce\play-services-tagmanager-api-9.2.1\AndroidManifest.xml:25:9-27:42
142            android:name="com.google.android.gms.tagmanager.TagManagerService"
142-->[com.google.android.gms:play-services-tagmanager-api:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1e454b2e27696a8d91b270823ea27fce\play-services-tagmanager-api-9.2.1\AndroidManifest.xml:25:18-84
143            android:enabled="true"
143-->[com.google.android.gms:play-services-tagmanager-api:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1e454b2e27696a8d91b270823ea27fce\play-services-tagmanager-api-9.2.1\AndroidManifest.xml:27:17-39
144            android:exported="false" />
144-->[com.google.android.gms:play-services-tagmanager-api:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1e454b2e27696a8d91b270823ea27fce\play-services-tagmanager-api-9.2.1\AndroidManifest.xml:26:17-41
145
146        <activity
146-->[com.google.android.gms:play-services-tagmanager-api:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1e454b2e27696a8d91b270823ea27fce\play-services-tagmanager-api-9.2.1\AndroidManifest.xml:28:9-37:20
147            android:name="com.google.android.gms.tagmanager.TagManagerPreviewActivity"
147-->[com.google.android.gms:play-services-tagmanager-api:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1e454b2e27696a8d91b270823ea27fce\play-services-tagmanager-api-9.2.1\AndroidManifest.xml:29:13-87
148            android:noHistory="true" > <!-- optional, removes the previewActivity from the activity stack. -->
148-->[com.google.android.gms:play-services-tagmanager-api:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1e454b2e27696a8d91b270823ea27fce\play-services-tagmanager-api-9.2.1\AndroidManifest.xml:30:13-37
149            <intent-filter>
149-->[com.google.android.gms:play-services-tagmanager-api:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1e454b2e27696a8d91b270823ea27fce\play-services-tagmanager-api-9.2.1\AndroidManifest.xml:31:13-36:29
150                <data android:scheme="tagmanager.c.com.androidrecipes.mapper.debug" />
150-->[com.google.android.gms:play-services-tagmanager-api:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1e454b2e27696a8d91b270823ea27fce\play-services-tagmanager-api-9.2.1\AndroidManifest.xml:32:17-72
150-->[com.google.android.gms:play-services-tagmanager-api:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1e454b2e27696a8d91b270823ea27fce\play-services-tagmanager-api-9.2.1\AndroidManifest.xml:32:23-69
151
152                <action android:name="android.intent.action.VIEW" />
152-->[com.google.android.gms:play-services-tagmanager-api:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1e454b2e27696a8d91b270823ea27fce\play-services-tagmanager-api-9.2.1\AndroidManifest.xml:33:17-69
152-->[com.google.android.gms:play-services-tagmanager-api:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1e454b2e27696a8d91b270823ea27fce\play-services-tagmanager-api-9.2.1\AndroidManifest.xml:33:25-66
153
154                <category android:name="android.intent.category.DEFAULT" />
154-->[com.google.android.gms:play-services-appinvite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\11fbf75f25d8968f9a0426837212aef1\play-services-appinvite-9.2.1\AndroidManifest.xml:27:17-75
154-->[com.google.android.gms:play-services-appinvite:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\11fbf75f25d8968f9a0426837212aef1\play-services-appinvite-9.2.1\AndroidManifest.xml:27:27-73
155                <category android:name="android.intent.category.BROWSABLE" />
155-->[com.google.android.gms:play-services-tagmanager-api:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1e454b2e27696a8d91b270823ea27fce\play-services-tagmanager-api-9.2.1\AndroidManifest.xml:35:17-77
155-->[com.google.android.gms:play-services-tagmanager-api:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\1e454b2e27696a8d91b270823ea27fce\play-services-tagmanager-api-9.2.1\AndroidManifest.xml:35:27-75
156            </intent-filter>
157        </activity>
158
159        <receiver
159-->[com.google.firebase:firebase-analytics:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2bb07d4fb3ba18ef17350ac62ad1d9f8\firebase-analytics-9.2.1\AndroidManifest.xml:28:7-34:18
160            android:name="com.google.android.gms.measurement.AppMeasurementReceiver"
160-->[com.google.firebase:firebase-analytics:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2bb07d4fb3ba18ef17350ac62ad1d9f8\firebase-analytics-9.2.1\AndroidManifest.xml:29:11-83
161            android:enabled="true" >
161-->[com.google.firebase:firebase-analytics:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2bb07d4fb3ba18ef17350ac62ad1d9f8\firebase-analytics-9.2.1\AndroidManifest.xml:30:11-33
162            <intent-filter>
162-->[com.google.firebase:firebase-analytics:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2bb07d4fb3ba18ef17350ac62ad1d9f8\firebase-analytics-9.2.1\AndroidManifest.xml:31:9-33:25
163                <action android:name="com.google.android.gms.measurement.UPLOAD" />
163-->[com.google.firebase:firebase-analytics:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2bb07d4fb3ba18ef17350ac62ad1d9f8\firebase-analytics-9.2.1\AndroidManifest.xml:32:11-77
163-->[com.google.firebase:firebase-analytics:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2bb07d4fb3ba18ef17350ac62ad1d9f8\firebase-analytics-9.2.1\AndroidManifest.xml:32:19-75
164            </intent-filter>
165        </receiver>
166
167        <service
167-->[com.google.firebase:firebase-analytics:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2bb07d4fb3ba18ef17350ac62ad1d9f8\firebase-analytics-9.2.1\AndroidManifest.xml:36:7-39:37
168            android:name="com.google.android.gms.measurement.AppMeasurementService"
168-->[com.google.firebase:firebase-analytics:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2bb07d4fb3ba18ef17350ac62ad1d9f8\firebase-analytics-9.2.1\AndroidManifest.xml:37:11-82
169            android:enabled="true"
169-->[com.google.firebase:firebase-analytics:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2bb07d4fb3ba18ef17350ac62ad1d9f8\firebase-analytics-9.2.1\AndroidManifest.xml:38:11-33
170            android:exported="false" />
170-->[com.google.firebase:firebase-analytics:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2bb07d4fb3ba18ef17350ac62ad1d9f8\firebase-analytics-9.2.1\AndroidManifest.xml:39:11-35
171
172        <receiver
172-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:32:9-41:20
173            android:name="com.google.firebase.iid.FirebaseInstanceIdReceiver"
173-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:33:13-78
174            android:exported="true"
174-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:34:13-36
175            android:permission="com.google.android.c2dm.permission.SEND" >
175-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:35:13-73
176            <intent-filter>
176-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:36:13-40:29
177                <action android:name="com.google.android.c2dm.intent.RECEIVE" />
177-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:37:17-81
177-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:37:25-78
178                <action android:name="com.google.android.c2dm.intent.REGISTRATION" />
178-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:38:17-86
178-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:38:25-83
179
180                <category android:name="com.androidrecipes.mapper.debug" />
180-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:39:17-61
180-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:39:27-58
181            </intent-filter>
182        </receiver>
183        <!--
184 Internal (not exported) receiver used by the app to start its own exported services
185             without risk of being spoofed.
186        -->
187        <receiver
187-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:45:9-47:40
188            android:name="com.google.firebase.iid.FirebaseInstanceIdInternalReceiver"
188-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:46:13-86
189            android:exported="false" />
189-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:47:13-37
190        <!--
191 FirebaseInstanceIdService performs security checks at runtime,
192             no need for explicit permissions despite exported="true"
193        -->
194        <service
194-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:51:9-57:19
195            android:name="com.google.firebase.iid.FirebaseInstanceIdService"
195-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:52:13-77
196            android:exported="true" >
196-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:53:13-36
197            <intent-filter android:priority="-500" >
197-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:54:13-56:29
197-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:54:28-51
198                <action android:name="com.google.firebase.INSTANCE_ID_EVENT" />
198-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:55:17-80
198-->[com.google.firebase:firebase-iid:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\2ce0d77963f5dc4ee6dde5032aa93dba\firebase-iid-9.2.1\AndroidManifest.xml:55:25-77
199            </intent-filter>
200        </service>
201
202        <provider
202-->[com.google.firebase:firebase-common:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\f4ed03a59e34b5288fb66472b306079b\firebase-common-9.2.1\AndroidManifest.xml:7:9-11:39
203            android:name="com.google.firebase.provider.FirebaseInitProvider"
203-->[com.google.firebase:firebase-common:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\f4ed03a59e34b5288fb66472b306079b\firebase-common-9.2.1\AndroidManifest.xml:9:13-58
204            android:authorities="com.androidrecipes.mapper.debug.firebaseinitprovider"
204-->[com.google.firebase:firebase-common:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\f4ed03a59e34b5288fb66472b306079b\firebase-common-9.2.1\AndroidManifest.xml:8:13-72
205            android:exported="false"
205-->[com.google.firebase:firebase-common:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\f4ed03a59e34b5288fb66472b306079b\firebase-common-9.2.1\AndroidManifest.xml:10:13-37
206            android:initOrder="100" />
206-->[com.google.firebase:firebase-common:9.2.1] C:\Users\Dell\.gradle\caches\transforms-2\files-2.1\f4ed03a59e34b5288fb66472b306079b\firebase-common-9.2.1\AndroidManifest.xml:11:13-36
207    </application>
208
209</manifest>
