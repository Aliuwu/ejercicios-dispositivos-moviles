package com.androidrecipes.mapper;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MarkerMapActivity extends FragmentActivity implements
        RadioGroup.OnCheckedChangeListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.InfoWindowAdapter, OnMapReadyCallback {
    private static final String TAG = "AndroidRecipes";
    private static final int REQUEST_CODE_PERMISSIONS = 10;

    private SupportMapFragment mMapFragment;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        //Verifica que play services esté activo
        int resultCode = GoogleApiAvailability.getInstance()
                .isGooglePlayServicesAvailable(this);
        switch (resultCode) {
            case ConnectionResult.SUCCESS:
                Log.d(TAG, "Google Play Services is ready to go!");
                break;
            default:
                showPlayServicesError(resultCode);
                return;
        }

        mMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        // Mapa vista normal
        RadioGroup typeSelect = (RadioGroup) findViewById(R.id.group_maptype);
        typeSelect.setEnabled(false);
        typeSelect.setOnCheckedChangeListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapFragment.getMapAsync(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMap = null;
    }

    /**
     * OnCheckedChangeListener Methods
     */
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.type_satellite:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.type_normal:
            default:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
        }
    }

    /**
     * OnMarkerClickListener Methods
     */

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    /**
     * OnMarkerDragListener Methods
     */

    @Override
    public void onMarkerDrag(Marker marker) {
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        Log.i("MarkerTest", "Drag " + marker.getTitle()
                + " to " + marker.getPosition());
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        Log.d("MarkerTest", "Drag " + marker.getTitle()
                + " from " + marker.getPosition());
    }

    /**
     * OnInfoWindowClickListener Methods
     */

    /**
     * Cierra la ventana
     * @param marker
     */
    @Override
    public void onInfoWindowClick(Marker marker) {
        marker.hideInfoWindow();
    }

    /**
     * InfoWindowAdapter Methods
     */


    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    /**
     * Información que se muestra en pantalla
     * @param marker
     */
    @Override
    public View getInfoWindow(Marker marker) {
        View content = createInfoView(marker);
        content.setBackgroundResource(R.drawable.background);
        return content;
    }

     /**
     * Para construir la vista
     * @param marker
     */
    private View createInfoView(Marker marker) {
        View content = getLayoutInflater().inflate(
                R.layout.info_window, null);
        ImageView image = (ImageView) content
                .findViewById(R.id.image);
        TextView text = (TextView) content
                .findViewById(R.id.text);

        image.setImageResource(R.drawable.ic_launcher);
        text.setText(marker.getTitle());

        return content;
    }


    /**
     * Cuadro de diálogo de ayuda
     * @param errorCode
     */
    private void showPlayServicesError(int errorCode) {
        GoogleApiAvailability.getInstance()
                .showErrorDialogFragment(this, errorCode, 1,
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                finish();
                            }
                        });
    }

    /**
     * Inicializa el mapa
     * @param googleMap
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        /**
         * Monitores de los marcadores y eventos
         */
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMarkerDragListener(this);
        mMap.setInfoWindowAdapter(this);
        mMap.setOnInfoWindowClickListener(this);

        // Google HQ 37.427,-122.099
        Marker marker = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.4218, -122.0840))
                .title("Google HQ")
                .icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.logo))
                //Reduce la opacidad
                .alpha(0.6f));
        //Para que el marcador pueda ser arrastrado
        marker.setDraggable(true);

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.4118, -122.0740))
                .title("Neighbor #1")
                .snippet("Best Restaurant in Town")
                // Marcador por defecto
                .icon(BitmapDescriptorFactory.defaultMarker()));

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.4318, -122.0940))
                .title("Neighbor #2")
                .snippet("Worst Restaurant in Town")
                // Marcador por defecto
                .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

        LatLng mapCenter = new LatLng(37.4218, -122.0840);
        CameraUpdate newCamera = CameraUpdateFactory
                .newLatLngZoom(mapCenter, 13);
        mMap.moveCamera(newCamera);

        RadioGroup typeSelect = (RadioGroup) findViewById(R.id.group_maptype);
        typeSelect.setEnabled(true);
    }
}
