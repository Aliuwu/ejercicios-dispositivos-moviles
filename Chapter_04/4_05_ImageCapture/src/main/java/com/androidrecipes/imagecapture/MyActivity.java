package com.androidrecipes.imagecapture;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;

/**
 * Problema: La aplicación necesita usar la cámara del dispositivo para capturas imágenes o videos cortos
 * Solución: Enviar un intent (mensaje) a Android para que trasfiera el control de la cámara y devuelva la imagen capturada por el usuario
 */
public class MyActivity extends Activity {

    private static final int REQUEST_IMAGE = 100;

    Button captureButton;
    ImageView imageView;
    File destination;

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        captureButton = (Button) findViewById(R.id.capture);
        captureButton.setOnClickListener(listener);

        imageView = (ImageView) findViewById(R.id.image);
        //Donde se guarda la imagen
        destination = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "image.jpg");
    }

    /**
     * Ajustes de la imagen capturada que será mostrada
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE && resultCode == Activity.RESULT_OK) {
            try {
                FileInputStream in = new FileInputStream(destination);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 10;
                Bitmap userImage = BitmapFactory.decodeStream(in, null, options);
                imageView.setImageBitmap(userImage);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * envía el intent para tomar control de la cámara
     */
    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            //Permisos para guardar la imagen
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(MyActivity.this, getApplicationContext().getPackageName() + ".provider", destination));
            Toast.makeText(MyActivity.this, FileProvider.getUriForFile(MyActivity.this, getApplicationContext().getPackageName() + ".provider", destination).toString(), Toast.LENGTH_SHORT).show();
            startActivityForResult(intent, REQUEST_IMAGE);
        }
    };
}

class GenericFileProvider extends FileProvider {}