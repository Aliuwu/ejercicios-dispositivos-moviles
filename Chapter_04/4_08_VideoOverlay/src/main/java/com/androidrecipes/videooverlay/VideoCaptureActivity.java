package com.androidrecipes.videooverlay;

import android.app.Activity;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.IOException;
/**
 * Problema: La aplicación requiere capturar video, pero se necesita una mayor control sobre el proceso de grabación que ejercicios anteriores
 * Solución: Usaremos MediaRecorder y la Cámara en conjunto.
 */

public class VideoCaptureActivity extends Activity implements SurfaceHolder.Callback {

    private Camera mCamera;
    private MediaRecorder mRecorder;

    private SurfaceView mPreview;
    private Button mRecordButton;

    private boolean mRecording = false;

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mRecordButton = (Button) findViewById(R.id.button_record);
        mRecordButton.setText("Start Recording");

        mPreview = (SurfaceView) findViewById(R.id.surface_video);
        mPreview.getHolder().addCallback(this);

        mCamera = Camera.open();
        //Para la vista previa
        mCamera.setDisplayOrientation(90);
        mRecorder = new MediaRecorder();
    }

    /**
     * destruye el proceso
     */
    @Override
    protected void onDestroy() {
        mCamera.release();
        mCamera = null;
        super.onDestroy();
    }

    /**
     * Actualiza el estado de la grabación
     * @param v
     */
    public void onRecordClick(View v) {
        updateRecordingState();
    }

    /**
     * Inicializa la cámara y la grabadora
     * @throws IllegalStateException
     * @throws IOException
     */
    private void initializeRecorder() throws IllegalStateException, IOException {
        //Desbloquea la cámara para ser usada por MediaRecorder
        mCamera.unlock();
        mRecorder.setCamera(mCamera);
        //Actualiza las configuraciones de entrada
        mRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        //Actualiza las configuraciones de salida
        File recordOutput =new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "recorded_video.mp4");
        if (recordOutput.exists()) {
            recordOutput.delete();
        }
        CamcorderProfile cpHigh = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
        mRecorder.setProfile(cpHigh);
        mRecorder.setOutputFile(recordOutput.getAbsolutePath());
        mRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());

        //Límite de grabación
        mRecorder.setMaxDuration(50000); // 50 segundos
        mRecorder.setMaxFileSize(5000000); // Aproximadamente 5 megabytes

        mRecorder.prepare();
    }

    /**
     * Actualiza el estado de grabación
     */
    private void updateRecordingState() {
        if (mRecording) {
            mRecording = false;
            //Reseteo
            mRecorder.stop();
            mRecorder.reset();
            mCamera.lock();
            mRecordButton.setText("Start Recording");
        } else {
            try {
                //Reseteo
                initializeRecorder();
                //Inicio de grabación
                mRecording = true;
                mRecorder.start();
                mRecordButton.setText("Stop Recording");
            } catch (Exception e) {
                //Error durante la grabación
                e.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }
}
