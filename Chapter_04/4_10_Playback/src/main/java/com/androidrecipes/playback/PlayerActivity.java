package com.androidrecipes.playback;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.MediaController;
/**
 * Problema: La aplicación necesita reproducir audio o video, ya sea de forma local o remota
 * Solución: Usaremos MediaPlayer, MediaController y VideoView
 */
public class PlayerActivity extends Activity implements
        MediaController.MediaPlayerControl, MediaPlayer.OnBufferingUpdateListener {

    MediaController mController;
    MediaPlayer mPlayer;
    ImageView coverImage;

    int bufferPercent = 0;

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        coverImage = (ImageView) findViewById(R.id.coverImage);

        mController = new MediaController(this);
        mController.setAnchorView(findViewById(R.id.root));
    }

    @Override
    public void onResume() {
        super.onResume();
        mPlayer = new MediaPlayer();
        //Asigna la fuente del audio
        try {
            mPlayer.setDataSource(this, Uri.parse("http://www.jingle.org/levysfurnishers.mp3"));
            mPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Asigna una imagen para la portada
        coverImage.setImageResource(R.drawable.ic_launcher);

        mController.setMediaPlayer(this);
        mController.setEnabled(true);
    }

    /**
     * Pausa la actividad
     */
    @Override
    public void onPause() {
        super.onPause();
        mPlayer.release();
        mPlayer = null;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mController.show();
        return super.onTouchEvent(event);
    }

    //Métodos de MediaPlayerControl
    @Override
    public int getBufferPercentage() {
        return bufferPercent;
    }

    /**
     * Obtiene la posición actual
     */
    @Override
    public int getCurrentPosition() {
        return mPlayer.getCurrentPosition();
    }

    /**
     * Obtiene la duración
     */
    @Override
    public int getDuration() {
        return mPlayer.getDuration();
    }

    /**
     * Para comprobar si se está reproduciendo
     */
    @Override
    public boolean isPlaying() {
        return mPlayer.isPlaying();
    }

    @Override
    public void pause() {
        mPlayer.pause();
    }

    @Override
    public void seekTo(int pos) {
        mPlayer.seekTo(pos);
    }

    @Override
    public void start() {
        mPlayer.start();
    }

    //Métodos de BufferUpdateListener
    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        bufferPercent = percent;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        return mPlayer.getAudioSessionId();
    }
}