package com.androidrecipes.playback;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

/**
 * Problema: La aplicación necesita reproducir audio o video, ya sea de forma local o remota
 * Solución: Usaremos MediaPlayer, MediaController y VideoView
 */
public class VideoActivity extends Activity {

    private static final boolean SHOULD_REDIRECT = false;

    VideoView videoView;
    MediaController controller;

    /**
     * Inicializa la actividad
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Conecta los componentes a la vista
        videoView = new VideoView(this);
        controller = new MediaController(this);
        videoView.setMediaController(controller);

        Uri videoLocation = Uri.parse("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4");
        if (SHOULD_REDIRECT) {

            RedirectTracerTask task = new RedirectTracerTask(videoView);
            task.execute(videoLocation);
        } else {
            //Para reproducir el video
            videoView.setVideoURI(videoLocation);
            videoView.start();
        }


        setContentView(videoView);
    }

    /**
     * Destruye el proceso
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        videoView.stopPlayback();
    }
}
