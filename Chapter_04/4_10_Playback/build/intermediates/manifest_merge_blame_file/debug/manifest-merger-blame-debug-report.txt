1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.androidrecipes.playback"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="14"
8-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml
9        android:targetSdkVersion="29" />
9-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml
10
11    <uses-permission android:name="android.permission.INTERNET" />
11-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:7:5-66
11-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:7:22-64
12
13    <application
13-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:9:5-29:19
14        android:debuggable="true"
15        android:icon="@drawable/ic_launcher"
15-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:10:9-45
16        android:label="@string/app_name"
16-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:11:9-41
17        android:testOnly="true"
18        android:theme="@style/AppTheme" >
18-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:12:9-40
19        <activity
19-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:13:9-20:20
20            android:name="com.androidrecipes.playback.PlayerActivity"
20-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:14:13-43
21            android:label="@string/label_audio" >
21-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:15:13-48
22            <intent-filter>
22-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:16:13-19:29
23                <action android:name="android.intent.action.MAIN" />
23-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:17:17-68
23-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:17:25-66
24
25                <category android:name="android.intent.category.LAUNCHER" />
25-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:18:17-76
25-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:18:27-74
26            </intent-filter>
27        </activity>
28        <activity
28-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:21:9-28:20
29            android:name="com.androidrecipes.playback.VideoActivity"
29-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:22:13-42
30            android:label="@string/label_video" >
30-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:23:13-48
31            <intent-filter>
31-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:16:13-19:29
32                <action android:name="android.intent.action.MAIN" />
32-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:17:17-68
32-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:17:25-66
33
34                <category android:name="android.intent.category.LAUNCHER" />
34-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:18:17-76
34-->C:\Users\Dell\AndroidStudioProjects\Chapter04_Source_Code\4_10_Playback\src\main\AndroidManifest.xml:18:27-74
35            </intent-filter>
36        </activity>
37    </application>
38
39</manifest>
