package com.androidrecipes.usermotionactivity;

import android.app.IntentService;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

public class UserMotionService extends IntentService {
    private static final String TAG = "UserMotionService";
    /**
     * Para detectar cambios en la actividad
     */
    public interface OnActivityChangedListener {
        public void onUserActivityChanged(int bestChoice, int bestConfidence,
                                          ActivityRecognitionResult newActivity);
    }

    /**
     * La última actividad detectada
     */
    private DetectedActivity mLastKnownActivity;

    private CallbackHandler mHandler;

    private static class CallbackHandler extends Handler {
        private OnActivityChangedListener mCallback;

        public void setCallback(OnActivityChangedListener callback) {
            mCallback = callback;
        }

        /**
         *Lee datos de carga útil del mensaje y devuelve la llamada
         * @param msg
         */
        @Override
        public void handleMessage(Message msg) {
            if (mCallback != null) {
                ActivityRecognitionResult newActivity = (ActivityRecognitionResult) msg.obj;
                mCallback.onUserActivityChanged(msg.arg1, msg.arg2, newActivity);
            }
        }
    }

    /**
     * Proceso en segundo plano
     */
    public UserMotionService() {
        super("UserMotionService");
        mHandler = new CallbackHandler();
    }

    public void setOnActivityChangedListener(OnActivityChangedListener listener) {
        mHandler.setCallback(listener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.w(TAG, "Service is stopping...");
    }

    /**
     * Eventos en segundo plano
     * @param intent
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        if (ActivityRecognitionResult.hasResult(intent)) {
            //Extraemos el resultado
            ActivityRecognitionResult result =
                    ActivityRecognitionResult.extractResult(intent);
            DetectedActivity activity = result.getMostProbableActivity();
            Log.v(TAG, "New User Activity Event");

            if (activity.getType() == DetectedActivity.UNKNOWN
                    && activity.getConfidence() < 60
                    && result.getProbableActivities().size() > 1) {
                    activity = result.getProbableActivities().get(1);
            }

            //Se activa cuando se producen cambios en la actividad
            if (mLastKnownActivity == null
                    || mLastKnownActivity.getType() != activity.getType()
                    || mLastKnownActivity.getConfidence() != activity.getConfidence()) {
                //Pasa los resultados al proceso principal
                Message msg = Message.obtain(null,
                        0,
                        activity.getType(),
                        activity.getConfidence(),
                        result);
                mHandler.sendMessage(msg);
            }
            mLastKnownActivity = activity;
        }
    }


    /**
     *Esto se llama cuando la actividad quiere vincularse al servicio.
     * @param intent
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private LocalBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public UserMotionService getService() {
            return UserMotionService.this;
        }
    }


    /**
     * Los nombres que se mostrarán para cada estado
     * @param activity
     */
    public static String getActivityName(DetectedActivity activity) {
        switch (activity.getType()) {
            case DetectedActivity.IN_VEHICLE:
                return "Driving";
            case DetectedActivity.ON_BICYCLE:
                return "Biking";
            case DetectedActivity.ON_FOOT:
                return "Walking";
            case DetectedActivity.STILL:
                return "Not Moving";
            case DetectedActivity.TILTING:
                return "Tilting";
            case DetectedActivity.UNKNOWN:
            default:
                return "No Clue";
        }
    }
}
